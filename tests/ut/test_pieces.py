from continuous_chess.datatypes import Colour, Coordinate
from continuous_chess.pieces import Piece, PieceType


class TestPiece:
    def test_piece_distance_from(self):
        """Check distance_from gives the right distance between pieces."""
        piece1 = Piece(PieceType.KING, Colour.BLACK, Coordinate(0.25, 0.25))
        piece2 = Piece(PieceType.KING, Colour.WHITE, Coordinate(3.25, 4.25))

        assert piece1.distance_from(piece2) == 5.0

__all__ = ("set_debug_level", "log")

from datetime import datetime
from typing import Any

# Anything of level >= DEBUG_LEVEL will be logged.
# Default is 0. Can be negative.
DEBUG_LEVEL = 0


def set_debug_level(level: int) -> None:
    global DEBUG_LEVEL
    DEBUG_LEVEL = level


def log(msg: Any, level=0) -> None:
    if level < DEBUG_LEVEL:
        return

    # Cut timestamp seconds to 2dp.
    timestamp = datetime.now().strftime("%d-%m-%Y, %H:%M %S.%f")[:24]
    print(f"{timestamp}  [{level}]  {msg}")

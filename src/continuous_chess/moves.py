__all__ = ("make_move",)

import copy
import enum
from math import sqrt
from typing import NamedTuple, Optional

from . import board, history, pieces
from . import game_log as glog
from .datatypes import Colour, Coordinate
from .pieces import PIECE_DIAMETER, PIECE_RADIUS, PieceType


def make_move(start: Coordinate, end: Coordinate) -> None:
    move = _Move(start, end)
    move.make_move()


class _Move:
    """
    An object representing a move. The move is not executed until the make_move()
    method is called.

    Attributes:
    - start
        Start location of the piece being moved.
    - end
        Location clicked as the end location of the piece.
    - piece
        The piece being moved.

    """

    def __init__(self, start: Coordinate, end: Coordinate):
        """
        The `start` and `end` coordinates are *click* locations. There may not
        necessarily be a piece exactly at `start`. (Though, it is expected there
        is a piece within PIECE_RADIUS of the click, which is the piece being
        moved.)

        """
        # Iterate through pieces to find the starting piece.
        for piece in history.current_state:
            if piece.location.distance_from(start) < PIECE_RADIUS:
                self.start = piece.location
                self.piece = piece
                break
        else:
            assert False, "Unable to init move: no piece found near first click."

        self.end = end

    @property
    def distance(self) -> float:
        return self.start.distance_from(self.end)

    def make_move(self) -> None:
        assert self.piece.colour is history.current_state.turn, \
            f"Cannot move piece {self.piece} out of turn!"
        if not self._is_valid():
            glog.log("Invalid move!", 2)
            return

        # Calculate new piece positions.
        # Piece objects must be copied, else history will be modified.
        new_piece_positions = []
        for old_piece in history.current_state:
            piece = copy.deepcopy(old_piece)

            if piece == self.piece:
                piece.location = self.end
                piece.moved = True
                new_piece_positions.append(piece)
            elif piece.distance_from(self.end) <= PIECE_DIAMETER:
                # Piece is captured; do not add it to new board state.
                glog.log(f"Piece captured: {piece}", 2)
                pass
            else:
                # Piece is not captured.
                new_piece_positions.append(piece)

        # Update game state.
        history.update_state_from_pieces(new_piece_positions)

    def _is_valid(self) -> bool:
        if self.distance < PIECE_RADIUS:
            glog.log("Movement too short.")
            return False
        if self.piece.piece_type is PieceType.PAWN:
            # Find coordinates of click location in reference frame where the
            # current piece location is the origin.
            x = self.end.x - self.start.x
            y = self.end.y - self.start.y

            if x > abs(2 * y):
                direction = Direction.R
            elif x < -abs(2 * y):
                direction = Direction.L
            elif y < -abs(2 * x):
                direction = Direction.U
            elif y > abs(2 * x):
                direction = Direction.D
            elif y < 0 and x > 0:
                direction = Direction.UR
            elif y < 0:
                direction = Direction.UL
            elif x < 0:
                direction = Direction.DL
            else:
                direction = Direction.DR

            if self.piece.colour is Colour.WHITE and direction not in (
                Direction.U,
                Direction.UL,
                Direction.UR,
            ):
                glog.log("Attempted to move Pawn in wrong direction.")
                return False
            if self.piece.colour is Colour.BLACK and direction not in (
                Direction.D,
                Direction.DL,
                Direction.DR,
            ):
                glog.log("Attempted to move Pawn in wrong direction.")
                return False

            # Snap end point to same row, column, or diagonal.
            if direction is Direction.D or direction is Direction.U:
                self.end = Coordinate(self.start.x, self.end.y)
            elif direction is Direction.DL or direction is Direction.UR:
                # "const" direction is y+x, "perp" direction is y-x.
                # Snap to location with click's perp and King's const.
                click_perp_k = self.end.y - self.end.x
                king_const_k = self.start.y + self.start.x
                # Need to solve y-x = click_perp_k, y+x = king_const_k
                # Subtract them to get: 2x = king_const_k - click_perp_k
                end_x = (king_const_k - click_perp_k) / 2
                end_y = click_perp_k + end_x
                self.end = Coordinate(end_x, end_y)
            else:
                # "const" direction is y-x, "perp" direction is y+x.
                # Snap to location with click's perp and King's const.
                click_perp_k = self.end.y + self.end.x
                king_const_k = self.start.y - self.start.x
                # Need to solve y+x = click_perp_k, y-x = king_const_k
                # Subtract them to get: 2x = click_perp_k - king_const_k
                end_x = (click_perp_k - king_const_k) / 2
                end_y = click_perp_k - end_x
                self.end = Coordinate(end_x, end_y)

            distance = self.start.distance_from(self.end)

            valid_distances = _valid_pawn_moves(self.piece)
            glog.log(
                f"Attempting to move pawn in direction {direction} distance "
                f"{distance}, maximum distances: {valid_distances}",
            )
            # Block invalid movement.
            if direction not in valid_distances:
                glog.log("Nothing to capture in this direction.")
                return False

            if not (
                valid_distances[direction].start
                < distance
                < valid_distances[direction].end
            ):
                glog.log(
                    f"Invalid movement distance, attempted to move {distance} "
                    f"but limit is [{valid_distances[direction].start}, "
                    f"{valid_distances[direction].end}]."
                )
                return False
        elif self.piece.piece_type is PieceType.ROOK:
            # Determine which direction the player wishes to move.
            # This is done by splitting the board into quadrants along the
            # diagonals through the rook and finding the quadrant the click is
            # in.
            down_distance = self.end.y - self.start.y
            right_distance = self.end.x - self.start.x
            if abs(down_distance) > abs(right_distance) and down_distance > 0:
                direction = Direction.D
            elif abs(down_distance) > abs(right_distance):
                direction = Direction.U
            elif right_distance > 0:
                direction = Direction.R
            else:
                direction = Direction.L

            # Snap end point to same row or column.
            if direction is Direction.D or direction is Direction.U:
                self.end = Coordinate(self.start.x, self.end.y)
                distance = abs(self.end.y - self.start.y)
            else:
                self.end = Coordinate(self.end.x, self.start.y)
                distance = abs(self.end.x - self.start.x)

            valid_distances = _valid_rook_moves(self.piece)
            glog.log(
                f"Attempting to move Rook in direction {direction} distance "
                f"{distance}, maximum distances: {valid_distances}",
            )
            # Block movement which is too far.
            if distance > valid_distances[direction].end:
                glog.log(
                    f"Movement too far, attempted to move {distance} "
                    f"but limit is {valid_distances[direction].end}"
                )
                return False
        elif self.piece.piece_type is PieceType.QUEEN:
            # Determine which direction the player wishes to move.
            # See UI.md for more information.

            # Find coordinates of click location in reference frame where the
            # current piece location is the origin.
            x = self.end.x - self.start.x
            y = self.end.y - self.start.y

            if x > abs(2 * y):
                direction = Direction.R
            elif x < -abs(2 * y):
                direction = Direction.L
            elif y < -abs(2 * x):
                direction = Direction.U
            elif y > abs(2 * x):
                direction = Direction.D
            elif y < 0 and x > 0:
                direction = Direction.UR
            elif y < 0:
                direction = Direction.UL
            elif x < 0:
                direction = Direction.DL
            else:
                direction = Direction.DR

            # Snap end point to same row, column, or diagonal.
            if direction is Direction.D or direction is Direction.U:
                self.end = Coordinate(self.start.x, self.end.y)
            elif direction is Direction.L or direction is Direction.R:
                self.end = Coordinate(self.end.x, self.start.y)
            elif direction is Direction.DL or direction is Direction.UR:
                # "const" direction is y+x, "perp" direction is y-x.
                # Snap to location with click's perp and Queen's const.
                click_perp_k = self.end.y - self.end.x
                queen_const_k = self.start.y + self.start.x
                # Need to solve y-x = click_perp_k, y+x = queen_const_k
                # Subtract them to get: 2x = queen_const_k - click_perp_k
                end_x = (queen_const_k - click_perp_k) / 2
                end_y = click_perp_k + end_x
                self.end = Coordinate(end_x, end_y)
            else:
                # "const" direction is y-x, "perp" direction is y+x.
                # Snap to location with click's perp and Queen's const.
                click_perp_k = self.end.y + self.end.x
                queen_const_k = self.start.y - self.start.x
                # Need to solve y+x = click_perp_k, y-x = queen_const_k
                # Subtract them to get: 2x = click_perp_k - queen_const_k
                end_x = (click_perp_k - queen_const_k) / 2
                end_y = click_perp_k - end_x
                self.end = Coordinate(end_x, end_y)

            distance = self.start.distance_from(self.end)

            valid_distances = _valid_queen_moves(self.piece)
            glog.log(
                f"Attempting to move Queen in direction {direction} distance "
                f"{distance}, maximum distances: {valid_distances}",
            )
            # Block movement which is too far.
            if distance > valid_distances[direction].end:
                glog.log(
                    f"Movement too far, attempted to move {distance} "
                    f"but limit is {valid_distances[direction].end}"
                )
                return False
        elif self.piece.piece_type is PieceType.KNIGHT:
            # Determine which direction the player wishes to move.

            # Find coordinates of click location in reference frame where the
            # current piece location is the origin.
            x = self.end.x - self.start.x
            y = self.end.y - self.start.y

            if y < 0 and x > -y:
                direction = Direction.RUR
            elif y < 0 and x > 0:
                direction = Direction.UUR
            elif y < 0 and y < x:
                direction = Direction.UUL
            elif y < 0:
                direction = Direction.LUL
            elif y < -x:
                direction = Direction.LDL
            elif x < 0:
                direction = Direction.DDL
            elif y > x:
                direction = Direction.DDR
            else:
                direction = Direction.RDR

            # Snap end location to a diagonal.
            if direction is Direction.RUR or direction is Direction.LDL:
                # Fix x-coordinate, snap y-coordinate.
                # Point must lie on y = -0.5x relative to start position.
                end_y_offset = -0.5 * x
                self.end = Coordinate(self.end.x, self.start.y + end_y_offset)
            elif direction is Direction.RDR or direction is Direction.LUL:
                # Fix x-coordinate, snap y-coordinate.
                # Point must lie on y = 0.5x relative to start position.
                end_y_offset = 0.5 * x
                self.end = Coordinate(self.end.x, self.start.y + end_y_offset)
            elif direction is Direction.UUR or direction is Direction.DDL:
                # Fix y-coordinate, snap x-coordinate.
                # Point must lie on x = -0.5y relative to start position.
                end_x_offset = -0.5 * y
                self.end = Coordinate(self.start.x + end_x_offset, self.end.y)
            else:
                # Fix y-coordinate, snap x-coordinate.
                # Point must lie on x = 0.5y relative to start position.
                end_x_offset = 0.5 * y
                self.end = Coordinate(self.start.x + end_x_offset, self.end.y)

            distance = self.start.distance_from(self.end)
            valid_regions = _valid_knight_moves(self.piece)[direction]
            glog.log(
                f"Attempting to move Knight in direction {direction} distance "
                f"{distance}.",
            )
            # Block movement which is invalid. Assume the movement is invalid,
            # unless a region is found where it is valid.
            is_movement_valid = False
            for region in valid_regions:
                if region.start < distance and distance < region.end:
                    is_movement_valid = True

            if not is_movement_valid:
                glog.log(
                    f"Attempted to move {distance} but this is blocked. "
                    "Valid regions: "
                    f"{[str(region) for region in valid_regions]}."
                )
                return False
        elif self.piece.piece_type is PieceType.BISHOP:
            # Determine which direction the player wishes to move.
            # See UI.md for more information.

            # Find coordinates of click location in reference frame where the
            # current piece location is the origin.
            x = self.end.x - self.start.x
            y = self.end.y - self.start.y

            if y < 0 and x > 0:
                direction = Direction.UR
            elif y < 0:
                direction = Direction.UL
            elif x < 0:
                direction = Direction.DL
            else:
                direction = Direction.DR

            # Snap end location to a diagonal.
            if direction is Direction.DL or direction is Direction.UR:
                # "const" direction is y+x, "perp" direction is y-x.
                # Snap to location with click's perp and Bishop's const.
                click_perp_k = self.end.y - self.end.x
                bishop_const_k = self.start.y + self.start.x
                # Need to solve y-x = click_perp_k, y+x = bishop_const_k
                # Subtract them to get: 2x = bishop_const_k - click_perp_k
                end_x = (bishop_const_k - click_perp_k) / 2
                end_y = click_perp_k + end_x
                self.end = Coordinate(end_x, end_y)
            else:
                # "const" direction is y-x, "perp" direction is y+x.
                # Snap to location with click's perp and Bishop's const.
                click_perp_k = self.end.y + self.end.x
                bishop_const_k = self.start.y - self.start.x
                # Need to solve y+x = click_perp_k, y-x = bishop_const_k
                # Subtract them to get: 2x = click_perp_k - bishop_const_k
                end_x = (click_perp_k - bishop_const_k) / 2
                end_y = click_perp_k - end_x
                self.end = Coordinate(end_x, end_y)

            distance = self.start.distance_from(self.end)
            valid_distances = _valid_bishop_moves(self.piece)
            glog.log(
                f"Attempting to move bishop in direction {direction} distance "
                f"{distance}, maximum distances: {valid_distances}",
            )
            # Block movement which is too far.
            if distance > valid_distances[direction].end:
                glog.log(
                    f"Movement too far, attempted to move {distance} "
                    f"but limit is {valid_distances[direction].end}"
                )
                return False
        elif self.piece.piece_type is PieceType.KING:
            # Determine which direction the player wishes to move.
            # See UI.md for more information.

            # Find coordinates of click location in reference frame where the
            # current piece location is the origin.
            x = self.end.x - self.start.x
            y = self.end.y - self.start.y

            if x > abs(2 * y):
                direction = Direction.R
            elif x < -abs(2 * y):
                direction = Direction.L
            elif y < -abs(2 * x):
                direction = Direction.U
            elif y > abs(2 * x):
                direction = Direction.D
            elif y < 0 and x > 0:
                direction = Direction.UR
            elif y < 0:
                direction = Direction.UL
            elif x < 0:
                direction = Direction.DL
            else:
                direction = Direction.DR

            # Snap end point to same row, column, or diagonal.
            if direction is Direction.D or direction is Direction.U:
                self.end = Coordinate(self.start.x, self.end.y)
            elif direction is Direction.L or direction is Direction.R:
                self.end = Coordinate(self.end.x, self.start.y)
            elif direction is Direction.DL or direction is Direction.UR:
                # "const" direction is y+x, "perp" direction is y-x.
                # Snap to location with click's perp and King's const.
                click_perp_k = self.end.y - self.end.x
                king_const_k = self.start.y + self.start.x
                # Need to solve y-x = click_perp_k, y+x = king_const_k
                # Subtract them to get: 2x = king_const_k - click_perp_k
                end_x = (king_const_k - click_perp_k) / 2
                end_y = click_perp_k + end_x
                self.end = Coordinate(end_x, end_y)
            else:
                # "const" direction is y-x, "perp" direction is y+x.
                # Snap to location with click's perp and King's const.
                click_perp_k = self.end.y + self.end.x
                king_const_k = self.start.y - self.start.x
                # Need to solve y+x = click_perp_k, y-x = king_const_k
                # Subtract them to get: 2x = click_perp_k - king_const_k
                end_x = (click_perp_k - king_const_k) / 2
                end_y = click_perp_k - end_x
                self.end = Coordinate(end_x, end_y)

            distance = self.start.distance_from(self.end)

            valid_distances = _valid_king_moves(self.piece)
            glog.log(
                f"Attempting to move king in direction {direction} distance "
                f"{distance}, maximum distances: {valid_distances}",
            )
            # Block movement which is too far.
            if distance > valid_distances[direction].end:
                glog.log(
                    f"Movement too far, attempted to move {distance} "
                    f"but limit is {valid_distances[direction].end}"
                )
                return False

        pieces_within_distance = board.pieces_within(PIECE_DIAMETER, self.end)
        if pieces_within_distance == []:
            glog.log("Moved to empty location.")
            return True
        else:
            for piece in pieces_within_distance:
                if piece.colour is self.piece.colour:
                    glog.log(f"Attempted to capture piece of own type: {piece}.")
                    return False

            glog.log("Moved to location overlapping enemy pieces.")
            return True

        raise RuntimeError("Unreachable.")


class Direction(enum.Enum):
    U = 0
    UR = 1
    R = 2
    DR = 3
    D = 4
    DL = 5
    L = 6
    UL = 7
    # The following are for knights only
    UUR = 8
    RUR = 9
    RDR = 10
    DDR = 11
    DDL = 12
    LDL = 13
    LUL = 14
    UUL = 15


class _Movement(NamedTuple):
    """Range of movement in an arbitrary direction."""
    start: float
    end: float


class _DistanceInfo(NamedTuple):
    """
    When a static piece is in the path of a moving piece, there is a distance
    at which the moving piece will just touch the static piece, and a (larger)
    distance at which the moving piece will move its maximum distance while
    still overlapping. This class stores these distances for a given piece.

    This data structure is used to calculate valid piece movement for
    non-Knight pieces.

    FIELDS
    colour:
        Colour of the static piece.

    touch_dist:
        Smallest distance the moving piece can move to touch the static piece.

    max_overlap_dist:
        Largest distance the moving piece can move and overlap the static
        piece.

    """

    colour: Colour
    touch_dist: float
    max_overlap_dist: float


class _BoundaryType(enum.Enum):
    """See _Boundary."""

    START = 0
    END = 1


class _Boundary(NamedTuple):
    """
    When a static piece is in the path of a moving piece, there is a start/end
    distance between which the moving piece overlaps the static piece. This
    class represents such a boundary.

    This data structure is used to calculate valid movement for Knights.

    FIELDS
    distance:
        Distance the moving piece has to move to reach this boundary point.

    boundary_type:
        Whether this is the start or end of the overlap.

    colour:
        Colour of the static piece.

    """

    distance: float
    boundary_type: _BoundaryType
    colour: Colour


###############################################################################
# VALID MOVEMENT OPTIONS CALCULATIONS
#
# Explanations of these algorithms are omitted here.
# For more information, see docs/Piece_movement.md.
###############################################################################


def _valid_up_or_down_move(
    moving_piece: pieces.Piece, direction: Direction
) -> _Movement:
    if not (direction is Direction.U or direction is Direction.D):
        raise ValueError("Direction must be U or D.")

    fwd_upper_bounds: list[float] = []
    # Append piece-specific movement bound.
    if moving_piece.piece_type is PieceType.KING:
        fwd_upper_bounds.append(1)
    elif moving_piece.piece_type is PieceType.PAWN:
        if moving_piece.colour is Colour.BLACK and direction is Direction.U:
            fwd_upper_bounds.append(0)
        elif moving_piece.colour is Colour.WHITE and direction is Direction.D:
            fwd_upper_bounds.append(0)
        else:
            fwd_upper_bounds.append(1 if moving_piece.moved else 2)

    distance_to_edge = (
        moving_piece.y - PIECE_DIAMETER
        if direction is Direction.U
        else (8 - PIECE_DIAMETER) - moving_piece.y
    )
    fwd_upper_bounds.append(distance_to_edge)

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(piece.x - moving_piece.x) < PIECE_DIAMETER
        and (
            (direction is direction.U and piece.y < moving_piece.y)
            or (direction is direction.D and piece.y > moving_piece.y)
        )
    ]
    # For pieces in the direction of movement, find the touching distance and
    # max overlap distance.
    distance_info: dict[pieces.Piece, _DistanceInfo] = {}
    for other_piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        touch_x_coord = (moving_piece.x + other_piece.x) / 2
        moving_piece_upper_y_coord = moving_piece.y - sqrt(
            PIECE_RADIUS**2 - (touch_x_coord - other_piece.x) ** 2
        )
        other_piece_lower_y_coord = other_piece.y + sqrt(
            PIECE_RADIUS**2 - (touch_x_coord - other_piece.x) ** 2
        )
        moving_piece_lower_y_coord = moving_piece.y + sqrt(
            PIECE_RADIUS**2 - (touch_x_coord - other_piece.x) ** 2
        )
        other_piece_upper_y_coord = other_piece.y - sqrt(
            PIECE_RADIUS**2 - (touch_x_coord - other_piece.x) ** 2
        )
        if direction is Direction.U:
            touch_distance = moving_piece_upper_y_coord - other_piece_lower_y_coord
            overlap_distance = moving_piece_lower_y_coord - other_piece_upper_y_coord
        else:
            touch_distance = other_piece_upper_y_coord - moving_piece_lower_y_coord
            overlap_distance = other_piece_lower_y_coord - moving_piece_upper_y_coord

        distance_info[other_piece] = _DistanceInfo(
            other_piece.colour,
            touch_distance,
            overlap_distance,
        )

    # Sort pieces by touch distance to find:
    # - closest same-colour piece.
    # - second-closest opposite-colour piece.
    # Do this in two iterations to simplify logic. No performance concerns due
    # to how few pieces there are.
    pieces_ahead.sort(key=lambda piece: distance_info[piece].touch_dist)
    for piece in pieces_ahead:
        if piece.colour is moving_piece.colour:
            fwd_upper_bounds.append(distance_info[piece].touch_dist)
            glog.log(
                f"Added upper bound for same-colour piece: {distance_info[piece].touch_dist}",
                level=-2,
            )
            break
    first_opposite_found = False
    for piece in pieces_ahead:
        if piece.colour is not moving_piece.colour and not first_opposite_found:
            # Ignore first encounted piece of same colour as it can be captured.
            first_opposite_found = True
        elif piece.colour is not moving_piece.colour:
            fwd_upper_bounds.append(distance_info[piece].touch_dist)
            glog.log(
                f"Added upper bound for opposite-colour blocker: {distance_info[piece].touch_dist}",
                level=-2,
            )
            break

    # Find the closest "max overlap distance". Note we cannot proceed past any
    # max overlap distances, regardless of piece colour.
    if len(pieces_ahead) > 0:
        closest_overlap = min(
            piece.max_overlap_dist for piece in distance_info.values()
        )
        fwd_upper_bounds.append(closest_overlap)
        glog.log(
            f"Added upper bound for overlap: {closest_overlap}",
            level=-2,
        )

    glog.log(f"In direction {direction}, {fwd_upper_bounds=}", level=-1)

    FWD_MOVEMENT = _Movement(
        start=0,
        end=min(fwd_upper_bounds),
    )

    return FWD_MOVEMENT


def _valid_left_or_right_move(
    moving_piece: pieces.Piece, direction: Direction
) -> _Movement:
    if not (direction is Direction.L or direction is Direction.R):
        raise ValueError("Direction must be L or R.")

    fwd_upper_bounds: list[float] = []
    if moving_piece.piece_type is PieceType.KING:
        fwd_upper_bounds.append(1)

    distance_to_edge = (
        moving_piece.x - PIECE_DIAMETER
        if direction is Direction.L
        else (8 - PIECE_DIAMETER) - moving_piece.x
    )
    fwd_upper_bounds.append(distance_to_edge)

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(piece.y - moving_piece.y) < PIECE_DIAMETER
        and (
            (direction is direction.L and piece.x < moving_piece.x)
            or (direction is direction.R and piece.x > moving_piece.x)
        )
    ]
    # For pieces in the direction of movement, find the touching distance and
    # max overlap distance.
    distance_info: dict[pieces.Piece, _DistanceInfo] = {}
    for other_piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        touch_y_coord = (moving_piece.y + other_piece.y) / 2
        moving_piece_left_x_coord = moving_piece.x - sqrt(
            PIECE_RADIUS**2 - (touch_y_coord - other_piece.y) ** 2
        )
        other_piece_right_x_coord = other_piece.x + sqrt(
            PIECE_RADIUS**2 - (touch_y_coord - other_piece.y) ** 2
        )
        moving_piece_right_x_coord = moving_piece.x + sqrt(
            PIECE_RADIUS**2 - (touch_y_coord - other_piece.y) ** 2
        )
        other_piece_left_x_coord = other_piece.x - sqrt(
            PIECE_RADIUS**2 - (touch_y_coord - other_piece.y) ** 2
        )
        if direction is Direction.L:
            touch_distance = moving_piece_left_x_coord - other_piece_right_x_coord
            overlap_distance = moving_piece_right_x_coord - other_piece_left_x_coord
        else:
            touch_distance = other_piece_left_x_coord - moving_piece_right_x_coord
            overlap_distance = other_piece_right_x_coord - moving_piece_left_x_coord

        distance_info[other_piece] = _DistanceInfo(
            other_piece.colour,
            touch_distance,
            overlap_distance,
        )

    # Sort pieces by touch distance to find:
    # - closest same-colour piece.
    # - second-closest opposite-colour piece.
    # Do this in two iterations to simplify logic. No performance concerns due
    # to how few pieces are being iterated over.
    pieces_ahead.sort(key=lambda piece: distance_info[piece].touch_dist)
    for piece in pieces_ahead:
        if piece.colour is moving_piece.colour:
            fwd_upper_bounds.append(distance_info[piece].touch_dist)
            glog.log(
                f"Added upper bound for same-colour piece: {distance_info[piece].touch_dist}",
                level=-2,
            )
            break
    first_opposite_found = False
    for piece in pieces_ahead:
        if piece.colour is not moving_piece.colour and not first_opposite_found:
            # Ignore first encounted piece of same colour as it can be captured.
            first_opposite_found = True
        elif piece.colour is not moving_piece.colour:
            fwd_upper_bounds.append(distance_info[piece].touch_dist)
            glog.log(
                f"Added upper bound for opposite-colour blocker: {distance_info[piece].touch_dist}",
                level=-2,
            )
            break

    # Find the closest "max overlap distance". Note we cannot proceed past any
    # max overlap distances, regardless of piece colour.
    if len(pieces_ahead) > 0:
        closest_overlap = min(
            piece.max_overlap_dist for piece in distance_info.values()
        )
        fwd_upper_bounds.append(closest_overlap)
        glog.log(
            f"Added upper bound for overlap: {closest_overlap}",
            level=-2,
        )

    glog.log(f"In direction {direction}, {fwd_upper_bounds=}", level=-1)

    FWD_MOVEMENT = _Movement(
        start=0,
        end=min(fwd_upper_bounds),
    )

    return FWD_MOVEMENT


def _valid_DL_UR_move(moving_piece: pieces.Piece, direction: Direction) -> _Movement:
    """
    Find range of valid movement in the direction along the DL-UR diagonal.

    """
    if not (direction is Direction.DL or direction is Direction.UR):
        raise ValueError("Direction must be DL or UR.")

    fwd_upper_bounds: list[float] = []
    if moving_piece.piece_type is PieceType.KING:
        fwd_upper_bounds.append(sqrt(2))

    distance_to_top_or_bottom_edge = (
        (moving_piece.y - PIECE_DIAMETER) * sqrt(2)
        if direction is Direction.UR
        else ((8 - PIECE_DIAMETER) - moving_piece.y) * sqrt(2)
    )
    distance_to_left_or_right_edge = (
        (moving_piece.x - PIECE_DIAMETER) * sqrt(2)
        if direction is Direction.DL
        else ((8 - PIECE_DIAMETER) - moving_piece.x) * sqrt(2)
    )
    fwd_upper_bounds.append(distance_to_top_or_bottom_edge)
    fwd_upper_bounds.append(distance_to_left_or_right_edge)

    # y+x is constant along lines of this gradient. (Remember the positive
    # y direction is DOWNWARDS.)
    def const(piece: pieces.Piece) -> float:
        return piece.y + piece.x

    def perp(piece: pieces.Piece) -> float:
        return piece.y - piece.x

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(const(piece) - const(moving_piece)) < 2 * sqrt(2) * PIECE_RADIUS
        and (
            (direction is direction.DL and perp(piece) > perp(moving_piece))
            or (direction is direction.UR and perp(piece) < perp(moving_piece))
        )
    ]
    glog.log(
        f"Moving {direction.name}. Pieces ahead of moving piece: "
        f"{[str(piece) for piece in pieces_ahead]}",
        level=-1,
    )

    # For pieces in the direction of movement, find the touching distance and
    # max overlap distance.
    distance_info: dict[pieces.Piece, _DistanceInfo] = {}
    for other_piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        # Variable names match what is written in Piece_movement.md.
        k_bar = (const(moving_piece) + const(other_piece)) / 2
        X = moving_piece.x
        Y = moving_piece.y
        U = other_piece.x
        V = other_piece.y
        r = PIECE_RADIUS

        t = k_bar - Y
        moving_piece_DL_x_coord = (X + t) / 2 - 0.5 * sqrt(2 * r**2 - (X - t) ** 2)
        moving_piece_UR_x_coord = (X + t) / 2 + 0.5 * sqrt(2 * r**2 - (X - t) ** 2)
        t = k_bar - V
        other_piece_DL_x_coord = (U + t) / 2 - 0.5 * sqrt(2 * r**2 - (U - t) ** 2)
        other_piece_UR_x_coord = (U + t) / 2 + 0.5 * sqrt(2 * r**2 - (U - t) ** 2)

        if direction is Direction.DL:
            touch_dist = (moving_piece_DL_x_coord - other_piece_UR_x_coord) * sqrt(2)
            overlap_dist = (moving_piece_UR_x_coord - other_piece_DL_x_coord) * sqrt(2)
        else:
            touch_dist = (other_piece_DL_x_coord - moving_piece_UR_x_coord) * sqrt(2)
            overlap_dist = (other_piece_UR_x_coord - moving_piece_DL_x_coord) * sqrt(2)

        distance_info[other_piece] = _DistanceInfo(
            other_piece.colour,
            touch_dist,
            overlap_dist,
        )

    # Sort pieces by touch distance to find:
    # - closest same-colour piece.
    # - second-closest opposite-colour piece.
    # Do this in two iterations to simplify logic. No performance concerns due
    # to how few pieces are being iterated over.
    pieces_ahead.sort(key=lambda piece: distance_info[piece].touch_dist)
    for piece in pieces_ahead:
        if piece.colour is moving_piece.colour:
            fwd_upper_bounds.append(distance_info[piece].touch_dist)
            glog.log(
                f"Added upper bound for same-colour piece: {distance_info[piece].touch_dist}",
                level=-2,
            )
            break
    first_opposite_found = False
    for piece in pieces_ahead:
        if piece.colour is not moving_piece.colour and not first_opposite_found:
            # Ignore first encounted piece of same colour as it can be captured.
            first_opposite_found = True
        elif piece.colour is not moving_piece.colour:
            fwd_upper_bounds.append(distance_info[piece].touch_dist)
            glog.log(
                f"Added upper bound for opposite-colour blocker: {distance_info[piece].touch_dist}",
                level=-2,
            )
            break

    # Find the closest "max overlap distance". Note we cannot proceed past any
    # max overlap distances, regardless of piece colour.
    if len(pieces_ahead) > 0:
        closest_overlap = min(
            piece.max_overlap_dist for piece in distance_info.values()
        )
        fwd_upper_bounds.append(closest_overlap)
        glog.log(
            f"Added upper bound for overlap: {closest_overlap}",
            level=-2,
        )

    glog.log(f"In direction {direction}, {fwd_upper_bounds=}", level=-1)

    FWD_MOVEMENT = _Movement(
        start=0,
        end=min(fwd_upper_bounds),
    )

    return FWD_MOVEMENT


def _valid_DL_UR_pawn_capture(
    moving_pawn: pieces.Piece, direction: Direction
) -> Optional[_Movement]:
    """
    Find range of valid movement in the direction along the DL-UR diagonal.

    """
    if not (direction is Direction.DL or direction is Direction.UR):
        raise ValueError("Direction must be DL or UR.")

    if moving_pawn.piece_type is not PieceType.PAWN:
        raise ValueError("Called Pawn capture calculation with non-Pawn piece.")

    # y+x is constant along lines of this gradient. (Remember the positive
    # y direction is DOWNWARDS.)
    def const(piece: pieces.Piece) -> float:
        return piece.y + piece.x

    def perp(piece: pieces.Piece) -> float:
        return piece.y - piece.x

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(const(piece) - const(moving_pawn)) < 2 * sqrt(2) * PIECE_RADIUS
        and (
            (direction is direction.DL and perp(piece) > perp(moving_pawn))
            or (direction is direction.UR and perp(piece) < perp(moving_pawn))
        )
    ]
    glog.log(
        f"Moving {direction.name}. Pieces ahead of moving piece: "
        f"{[str(piece) for piece in pieces_ahead]}",
        level=-1,
    )
    if not pieces_ahead:
        # No pieces capturable, so cannot move in this direction.
        return None

    distance_to_top_or_bottom_edge = (
        (moving_pawn.y - PIECE_DIAMETER) * sqrt(2)
        if direction is Direction.UR
        else ((8 - PIECE_DIAMETER) - moving_pawn.y) * sqrt(2)
    )
    distance_to_left_or_right_edge = (
        (moving_pawn.x - PIECE_DIAMETER) * sqrt(2)
        if direction is Direction.DL
        else ((8 - PIECE_DIAMETER) - moving_pawn.x) * sqrt(2)
    )
    # Cannot move further than sqrt(2), the Pawn's max movement distance when capturing,
    # or the edge of the board.
    max_distance = min(
        sqrt(2), distance_to_left_or_right_edge, distance_to_top_or_bottom_edge
    )

    # For each piece, find the movement distance values between which the Pawn
    # overlaps the piece. Store these distances (separately), along with whether
    # the distance is the start or end of an overlap range, and the piece colour.
    range_boundaries: list[_Boundary] = []
    for other_piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        # Variable names match what is written in Piece_movement.md.
        k_bar = (const(moving_pawn) + const(other_piece)) / 2
        X = moving_pawn.x
        Y = moving_pawn.y
        U = other_piece.x
        V = other_piece.y
        r = PIECE_RADIUS

        t = k_bar - Y
        moving_pawn_DL_x_coord = (X + t) / 2 - 0.5 * sqrt(2 * r**2 - (X - t) ** 2)
        moving_pawn_UR_x_coord = (X + t) / 2 + 0.5 * sqrt(2 * r**2 - (X - t) ** 2)
        t = k_bar - V
        other_piece_DL_x_coord = (U + t) / 2 - 0.5 * sqrt(2 * r**2 - (U - t) ** 2)
        other_piece_UR_x_coord = (U + t) / 2 + 0.5 * sqrt(2 * r**2 - (U - t) ** 2)

        if direction is Direction.DL:
            overlap_start_dist = (
                moving_pawn_DL_x_coord - other_piece_UR_x_coord
            ) * sqrt(2)
            overlap_end_dist = (moving_pawn_UR_x_coord - other_piece_DL_x_coord) * sqrt(
                2
            )
        else:
            overlap_start_dist = (
                other_piece_DL_x_coord - moving_pawn_UR_x_coord
            ) * sqrt(2)
            overlap_end_dist = (other_piece_UR_x_coord - moving_pawn_DL_x_coord) * sqrt(
                2
            )

        assert overlap_end_dist >= overlap_start_dist

        range_boundaries.append(
            _Boundary(
                overlap_start_dist,
                _BoundaryType.START,
                other_piece.colour,
            )
        )
        range_boundaries.append(
            _Boundary(
                overlap_end_dist,
                _BoundaryType.END,
                other_piece.colour,
            )
        )

    assert len(range_boundaries) >= 2
    range_boundaries.sort(key=lambda boundary: boundary.distance)

    if range_boundaries[0].colour is moving_pawn.colour:
        # Blocked by piece of own colour.
        return None

    if range_boundaries[0].distance > max_distance:
        # Would reach edge of board before any pieces.
        return None

    # If this is reached, there is a capturable piece.
    range_end = min(range_boundaries[1].distance, max_distance)

    return _Movement(start=range_boundaries[0].distance, end=range_end)


def _valid_UL_DR_pawn_capture(
    moving_pawn: pieces.Piece, direction: Direction
) -> Optional[_Movement]:
    """
    Find range of valid movement in the direction along the UL-DR diagonal.

    """
    if not (direction is Direction.UL or direction is Direction.DR):
        raise ValueError("Direction must be UL or DR.")

    if moving_pawn.piece_type is not PieceType.PAWN:
        raise ValueError("Called Pawn capture calculation with non-Pawn piece.")

    # y-x is constant along lines of this gradient. (Remember the positive
    # y direction is DOWNWARDS.)
    def const(piece: pieces.Piece) -> float:
        return piece.y - piece.x

    def perp(piece: pieces.Piece) -> float:
        return piece.y + piece.x

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(const(piece) - const(moving_pawn)) < 2 * sqrt(2) * PIECE_RADIUS
        and (
            (direction is direction.UL and perp(piece) < perp(moving_pawn))
            or (direction is direction.DR and perp(piece) > perp(moving_pawn))
        )
    ]
    glog.log(
        f"Moving {direction.name}. Pieces ahead of moving piece: "
        f"{[str(piece) for piece in pieces_ahead]}",
        level=-1,
    )
    if not pieces_ahead:
        # No pieces capturable, so cannot move in this direction.
        return None

    distance_to_top_or_bottom_edge = (
        (moving_pawn.y - PIECE_DIAMETER) * sqrt(2)
        if direction is Direction.UL
        else ((8 - PIECE_DIAMETER) - moving_pawn.y) * sqrt(2)
    )
    distance_to_left_or_right_edge = (
        (moving_pawn.x - PIECE_DIAMETER) * sqrt(2)
        if direction is Direction.DR
        else ((8 - PIECE_DIAMETER) - moving_pawn.x) * sqrt(2)
    )
    # Cannot move further than sqrt(2), the Pawn's max movement distance when capturing,
    # or the edge of the board.
    max_distance = min(
        sqrt(2), distance_to_left_or_right_edge, distance_to_top_or_bottom_edge
    )

    # For each piece, find the movement distance values between which the Pawn
    # overlaps the piece. Store these distances (separately), along with whether
    # the distance is the start or end of an overlap range, and the piece colour.
    range_boundaries: list[_Boundary] = []
    for other_piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        # Variable names match what is written in Piece_movement.md.
        k_bar = (const(moving_pawn) + const(other_piece)) / 2
        X = moving_pawn.x
        Y = moving_pawn.y
        U = other_piece.x
        V = other_piece.y
        r = PIECE_RADIUS

        t = Y - k_bar
        moving_pawn_UL_x_coord = (X + t) / 2 - 0.5 * sqrt(2 * r**2 - (X - t) ** 2)
        moving_pawn_DR_x_coord = (X + t) / 2 + 0.5 * sqrt(2 * r**2 - (X - t) ** 2)
        t = V - k_bar
        other_piece_UL_x_coord = (U + t) / 2 - 0.5 * sqrt(2 * r**2 - (U - t) ** 2)
        other_piece_DR_x_coord = (U + t) / 2 + 0.5 * sqrt(2 * r**2 - (U - t) ** 2)

        if direction is Direction.UL:
            overlap_start_dist = (
                moving_pawn_UL_x_coord - other_piece_DR_x_coord
            ) * sqrt(2)
            overlap_end_dist = (moving_pawn_DR_x_coord - other_piece_UL_x_coord) * sqrt(
                2
            )
        else:
            overlap_start_dist = (
                other_piece_UL_x_coord - moving_pawn_DR_x_coord
            ) * sqrt(2)
            overlap_end_dist = (other_piece_DR_x_coord - moving_pawn_UL_x_coord) * sqrt(
                2
            )

        assert overlap_end_dist >= overlap_start_dist

        range_boundaries.append(
            _Boundary(
                overlap_start_dist,
                _BoundaryType.START,
                other_piece.colour,
            )
        )
        range_boundaries.append(
            _Boundary(
                overlap_end_dist,
                _BoundaryType.END,
                other_piece.colour,
            )
        )

    assert len(range_boundaries) >= 2
    range_boundaries.sort(key=lambda boundary: boundary.distance)

    if range_boundaries[0].colour is moving_pawn.colour:
        # Blocked by piece of own colour.
        return None

    if range_boundaries[0].distance > max_distance:
        # Would reach edge of board before any pieces.
        return None

    # If this is reached, there is a capturable piece.
    range_end = min(range_boundaries[1].distance, max_distance)

    return _Movement(start=range_boundaries[0].distance, end=range_end)


def _valid_UL_DR_move(moving_piece: pieces.Piece, direction: Direction) -> _Movement:
    """
    Find range of valid movement in the direction along the UL-DR diagonal.

    """
    if not (direction is Direction.UL or direction is Direction.DR):
        raise ValueError("Direction must be UL or DR.")

    fwd_upper_bounds: list[float] = []
    if moving_piece.piece_type is PieceType.KING:
        fwd_upper_bounds.append(sqrt(2))

    distance_to_top_or_bottom_edge = (
        (moving_piece.y - PIECE_DIAMETER) * sqrt(2)
        if direction is Direction.UL
        else ((8 - PIECE_DIAMETER) - moving_piece.y) * sqrt(2)
    )
    distance_to_left_or_right_edge = (
        (moving_piece.x - PIECE_DIAMETER) * sqrt(2)
        if direction is Direction.UL
        else ((8 - PIECE_DIAMETER) - moving_piece.x) * sqrt(2)
    )
    fwd_upper_bounds.append(distance_to_top_or_bottom_edge)
    fwd_upper_bounds.append(distance_to_left_or_right_edge)

    # y-x is constant along lines of this gradient. (Remember the positive
    # y direction is DOWNWARDS.)
    def const(piece: pieces.Piece) -> float:
        return piece.y - piece.x

    def perp(piece: pieces.Piece) -> float:
        return piece.y + piece.x

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(const(piece) - const(moving_piece)) < 2 * sqrt(2) * PIECE_RADIUS
        and (
            (direction is direction.UL and perp(piece) < perp(moving_piece))
            or (direction is direction.DR and perp(piece) > perp(moving_piece))
        )
    ]
    glog.log(
        f"Moving {direction.name}. Pieces ahead of moving piece: "
        f"{[str(piece) for piece in pieces_ahead]}",
        level=-1,
    )

    # For pieces in the direction of movement, find the touching distance and
    # max overlap distance.
    distance_info: dict[pieces.Piece, _DistanceInfo] = {}
    for other_piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        # Variable names match what is written in Piece_movement.md.
        k_bar = (const(moving_piece) + const(other_piece)) / 2
        X = moving_piece.x
        Y = moving_piece.y
        U = other_piece.x
        V = other_piece.y
        r = PIECE_RADIUS

        t = Y - k_bar
        moving_piece_UL_x_coord = (X + t) / 2 - 0.5 * sqrt(2 * r**2 - (X - t) ** 2)
        moving_piece_DR_x_coord = (X + t) / 2 + 0.5 * sqrt(2 * r**2 - (X - t) ** 2)
        t = V - k_bar
        other_piece_UL_x_coord = (U + t) / 2 - 0.5 * sqrt(2 * r**2 - (U - t) ** 2)
        other_piece_DR_x_coord = (U + t) / 2 + 0.5 * sqrt(2 * r**2 - (U - t) ** 2)

        if direction is Direction.UL:
            touch_dist = (moving_piece_UL_x_coord - other_piece_DR_x_coord) * sqrt(2)
            overlap_dist = (moving_piece_DR_x_coord - other_piece_UL_x_coord) * sqrt(2)
        else:
            touch_dist = (other_piece_UL_x_coord - moving_piece_DR_x_coord) * sqrt(2)
            overlap_dist = (other_piece_DR_x_coord - moving_piece_UL_x_coord) * sqrt(2)

        distance_info[other_piece] = _DistanceInfo(
            other_piece.colour,
            touch_dist,
            overlap_dist,
        )

    # Sort pieces by touch distance to find:
    # - closest same-colour piece.
    # - second-closest opposite-colour piece.
    # Do this in two iterations to simplify logic. No performance concerns due
    # to how few pieces are being iterated over.
    pieces_ahead.sort(key=lambda piece: distance_info[piece].touch_dist)
    for piece in pieces_ahead:
        if piece.colour is moving_piece.colour:
            fwd_upper_bounds.append(distance_info[piece].touch_dist)
            glog.log(
                f"Added upper bound for same-colour piece: {distance_info[piece].touch_dist}",
                level=-2,
            )
            break
    first_opposite_found = False
    for piece in pieces_ahead:
        if piece.colour is not moving_piece.colour and not first_opposite_found:
            # Ignore first encounted piece of same colour as it can be captured.
            first_opposite_found = True
        elif piece.colour is not moving_piece.colour:
            fwd_upper_bounds.append(distance_info[piece].touch_dist)
            glog.log(
                f"Added upper bound for opposite-colour blocker: {distance_info[piece].touch_dist}",
                level=-2,
            )
            break

    # Find the closest "max overlap distance". Note we cannot proceed past any
    # max overlap distances, regardless of piece colour.
    if len(pieces_ahead) > 0:
        closest_overlap = min(
            piece.max_overlap_dist for piece in distance_info.values()
        )
        fwd_upper_bounds.append(closest_overlap)
        glog.log(
            f"Added upper bound for overlap: {closest_overlap}",
            level=-2,
        )

    glog.log(f"In direction {direction}, {fwd_upper_bounds=}", level=-1)

    FWD_MOVEMENT = _Movement(
        start=0,
        end=min(fwd_upper_bounds),
    )

    return FWD_MOVEMENT


def _valid_UUL_DDR_moves(knight: pieces.Piece, direction: Direction) -> list[_Movement]:
    """
    Find ranges of valid Knight movement in the direction along the UUL-DDR diagonal.

    This is "Case A" in Piece_movement.md.

    """
    if not (direction is Direction.UUL or direction is Direction.DDR):
        raise ValueError("Direction must be UUL or DDR.")

    distance_to_top_or_bottom_edge = (
        (knight.y - PIECE_DIAMETER) * sqrt(5) / 2
        if direction is Direction.UUL
        else ((8 - PIECE_DIAMETER) - knight.y) * sqrt(5) / 2
    )
    distance_to_left_or_right_edge = (
        (knight.x - PIECE_DIAMETER) * sqrt(5)
        if direction is Direction.UUL
        else ((8 - PIECE_DIAMETER) - knight.x) * sqrt(5)
    )
    max_distance = min(
        sqrt(5), distance_to_left_or_right_edge, distance_to_top_or_bottom_edge
    )

    # y-2x is constant along lines of this gradient. (Remember the positive
    # y direction is DOWNWARDS.)
    def const(piece: pieces.Piece) -> float:
        return piece.y - 2 * piece.x

    def perp(piece: pieces.Piece) -> float:
        return piece.y + 0.5 * piece.x

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(const(piece) - const(knight)) < 2 * sqrt(5) * PIECE_RADIUS
        and (
            (direction is direction.UUL and perp(piece) < perp(knight))
            or (direction is direction.DDR and perp(piece) > perp(knight))
        )
    ]
    glog.log(
        f"In direction {direction.name}, pieces ahead of moving piece: "
        f"{[str(piece) for piece in pieces_ahead]}",
        level=-1,
    )

    # For each piece, find the movement distance values between which the Knight
    # overlaps the piece. Store these distances (separately), along with whether
    # the distance is the start or end of an overlap range, and the piece colour.
    range_boundaries: list[_Boundary] = []
    for piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        # Variable names match what is written in Piece_movement.md.
        k_bar = (const(knight) + const(piece)) / 2
        X = knight.x
        Y = knight.y
        U = piece.x
        V = piece.y
        r = PIECE_RADIUS

        t = Y - k_bar
        knight_UL_x_coord = (X + 2 * t) / 5 - 0.2 * sqrt(5 * r**2 - (2 * X - t) ** 2)
        knight_DR_x_coord = (X + 2 * t) / 5 + 0.2 * sqrt(5 * r**2 - (2 * X - t) ** 2)
        t = V - k_bar
        piece_UL_x_coord = (U + 2 * t) / 5 - 0.2 * sqrt(5 * r**2 - (2 * U - t) ** 2)
        piece_DR_x_coord = (U + 2 * t) / 5 + 0.2 * sqrt(5 * r**2 - (2 * U - t) ** 2)

        if direction is Direction.UUL:
            overlap_start_dist = (knight_UL_x_coord - piece_DR_x_coord) * sqrt(5)
            overlap_end_dist = (knight_DR_x_coord - piece_UL_x_coord) * sqrt(5)
        else:
            overlap_start_dist = (piece_UL_x_coord - knight_DR_x_coord) * sqrt(5)
            overlap_end_dist = (piece_DR_x_coord - knight_UL_x_coord) * sqrt(5)

        assert overlap_end_dist >= overlap_start_dist

        range_boundaries.append(
            _Boundary(
                overlap_start_dist,
                _BoundaryType.START,
                piece.colour,
            )
        )
        range_boundaries.append(
            _Boundary(
                overlap_end_dist,
                _BoundaryType.END,
                piece.colour,
            )
        )

    range_boundaries.sort(key=lambda boundary: boundary.distance)

    # Prepare for iterating over the list by initialising the following:
    # Counters tracking how many same/opposite-colour pieces we are overlapping
    same_colour_overlaps = 0
    opp_colour_overlaps = 0
    # Indicator if the current region is valid to move to.
    in_valid_region = True
    # Start of the current valid region, when we in a valid region.
    region_start = 0
    # List of valid regions.
    valid_regions: list[_Movement] = []

    # Now iterate.
    for boundary in range_boundaries:
        if boundary.distance > max_distance:
            break

        if (
            boundary.boundary_type is _BoundaryType.START
            and boundary.colour is knight.colour
        ):
            same_colour_overlaps += 1
        elif boundary.boundary_type is _BoundaryType.START:
            opp_colour_overlaps += 1
        elif (
            boundary.boundary_type is _BoundaryType.END
            and boundary.colour is knight.colour
        ):
            assert same_colour_overlaps > 0
            same_colour_overlaps -= 1
        else:
            assert opp_colour_overlaps > 0
            opp_colour_overlaps -= 1

        if in_valid_region and (same_colour_overlaps > 0 or opp_colour_overlaps > 1):
            # End of valid region.
            in_valid_region = False
            # Filter out fake regions caused by float imprecision.
            if boundary.distance - region_start > 1e-5:
                valid_regions.append(
                    _Movement(region_start, boundary.distance)
                )
        elif not in_valid_region and (
            same_colour_overlaps == 0 and opp_colour_overlaps < 2
        ):
            # Start of valid region.
            in_valid_region = True
            region_start = boundary.distance

    # If in a valid region by the end, close off the end of this region.
    if in_valid_region:
        valid_regions.append(_Movement(region_start, max_distance))

    return valid_regions


def _valid_LUL_RDR_moves(knight: pieces.Piece, direction: Direction) -> list[_Movement]:
    """
    Find ranges of valid Knight movement in the direction along the LUL-RDR diagonal.

    This is "Case B" in Piece_movement.md.

    """
    if not (direction is Direction.LUL or direction is Direction.RDR):
        raise ValueError("Direction must be LUL or RDR.")

    distance_to_top_or_bottom_edge = (
        (knight.y - PIECE_DIAMETER) * sqrt(5)
        if direction is Direction.LUL
        else ((8 - PIECE_DIAMETER) - knight.y) * sqrt(5)
    )
    distance_to_left_or_right_edge = (
        (knight.x - PIECE_DIAMETER) * sqrt(5) / 2
        if direction is Direction.LUL
        else ((8 - PIECE_DIAMETER) - knight.x) * sqrt(5) / 2
    )
    max_distance = min(
        sqrt(5), distance_to_left_or_right_edge, distance_to_top_or_bottom_edge
    )

    # y-0.5x is constant along lines of this gradient. (Remember the positive
    # y direction is DOWNWARDS.)
    def const(piece: pieces.Piece) -> float:
        return piece.y - 0.5 * piece.x

    def perp(piece: pieces.Piece) -> float:
        return piece.y + 2 * piece.x

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(const(piece) - const(knight)) < sqrt(5) * PIECE_RADIUS
        and (
            (direction is direction.LUL and perp(piece) < perp(knight))
            or (direction is direction.RDR and perp(piece) > perp(knight))
        )
    ]
    glog.log(
        f"In direction {direction.name}, pieces ahead of moving piece: "
        f"{[str(piece) for piece in pieces_ahead]}",
        level=-1,
    )

    # For each piece, find the movement distance values between which the Knight
    # overlaps the piece. Store these distances (separately), along with whether
    # the distance is the start or end of an overlap range, and the piece colour.
    range_boundaries: list[_Boundary] = []
    for piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        # Variable names match what is written in Piece_movement.md.
        k_bar = (const(knight) + const(piece)) / 2
        X = knight.x
        Y = knight.y
        U = piece.x
        V = piece.y
        r = PIECE_RADIUS

        t = Y - k_bar
        knight_UL_x_coord = (4 * X + 2 * t) / 5 - 0.4 * sqrt(
            5 * r**2 - (X - 2 * t) ** 2
        )
        knight_DR_x_coord = (4 * X + 2 * t) / 5 + 0.4 * sqrt(
            5 * r**2 - (X - 2 * t) ** 2
        )
        t = V - k_bar
        piece_UL_x_coord = (4 * U + 2 * t) / 5 - 0.4 * sqrt(5 * r**2 - (U - 2 * t) ** 2)
        piece_DR_x_coord = (4 * U + 2 * t) / 5 + 0.4 * sqrt(5 * r**2 - (U - 2 * t) ** 2)

        if direction is Direction.LUL:
            overlap_start_dist = (knight_UL_x_coord - piece_DR_x_coord) * sqrt(5) / 2
            overlap_end_dist = (knight_DR_x_coord - piece_UL_x_coord) * sqrt(5) / 2
        else:
            overlap_start_dist = (piece_UL_x_coord - knight_DR_x_coord) * sqrt(5) / 2
            overlap_end_dist = (piece_DR_x_coord - knight_UL_x_coord) * sqrt(5) / 2

        assert overlap_end_dist >= overlap_start_dist

        range_boundaries.append(
            _Boundary(
                overlap_start_dist,
                _BoundaryType.START,
                piece.colour,
            )
        )
        range_boundaries.append(
            _Boundary(
                overlap_end_dist,
                _BoundaryType.END,
                piece.colour,
            )
        )

    range_boundaries.sort(key=lambda boundary: boundary.distance)

    # Prepare for iterating over the list by initialising the following:
    # Counters tracking how many same/opposite-colour pieces we are overlapping
    same_colour_overlaps = 0
    opp_colour_overlaps = 0
    # Indicator if the current region is valid to move to.
    in_valid_region = True
    # Start of the current valid region, when we in a valid region.
    region_start = 0
    # List of valid regions.
    valid_regions: list[_Movement] = []

    # Now iterate.
    for boundary in range_boundaries:
        if boundary.distance > max_distance:
            break

        if (
            boundary.boundary_type is _BoundaryType.START
            and boundary.colour is knight.colour
        ):
            same_colour_overlaps += 1
        elif boundary.boundary_type is _BoundaryType.START:
            opp_colour_overlaps += 1
        elif (
            boundary.boundary_type is _BoundaryType.END
            and boundary.colour is knight.colour
        ):
            assert same_colour_overlaps > 0
            same_colour_overlaps -= 1
        else:
            assert opp_colour_overlaps > 0
            opp_colour_overlaps -= 1

        if in_valid_region and (same_colour_overlaps > 0 or opp_colour_overlaps > 1):
            # End of valid region.
            in_valid_region = False
            # Filter out fake regions caused by float imprecision.
            if boundary.distance - region_start > 1e-5:
                valid_regions.append(
                    _Movement(region_start, boundary.distance)
                )
        elif not in_valid_region and (
            same_colour_overlaps == 0 and opp_colour_overlaps < 2
        ):
            # Start of valid region.
            in_valid_region = True
            region_start = boundary.distance

    # If in a valid region by the end, close off the end of this region.
    if in_valid_region:
        valid_regions.append(_Movement(region_start, max_distance))

    return valid_regions


def _valid_UUR_DDL_moves(knight: pieces.Piece, direction: Direction) -> list[_Movement]:
    """
    Find ranges of valid Knight movement in the direction along the UUR-DDL diagonal.

    This is "Case C" in Piece_movement.md.

    """
    if not (direction is Direction.UUR or direction is Direction.DDL):
        raise ValueError("Direction must be UUR or DDL.")

    distance_to_top_or_bottom_edge = (
        (knight.y - PIECE_DIAMETER) * sqrt(5) / 2
        if direction is Direction.UUR
        else ((8 - PIECE_DIAMETER) - knight.y) * sqrt(5) / 2
    )
    distance_to_left_or_right_edge = (
        (knight.x - PIECE_DIAMETER) * sqrt(5)
        if direction is Direction.DDL
        else ((8 - PIECE_DIAMETER) - knight.x) * sqrt(5)
    )
    max_distance = min(
        sqrt(5), distance_to_left_or_right_edge, distance_to_top_or_bottom_edge
    )

    # y+2x is constant along lines of this gradient. (Remember the positive
    # y direction is DOWNWARDS.)
    def const(piece: pieces.Piece) -> float:
        return piece.y + 2 * piece.x

    def perp(piece: pieces.Piece) -> float:
        return piece.y - 0.5 * piece.x

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(const(piece) - const(knight)) < 2 * sqrt(5) * PIECE_RADIUS
        and (
            (direction is direction.UUR and perp(piece) < perp(knight))
            or (direction is direction.DDL and perp(piece) > perp(knight))
        )
    ]
    glog.log(
        f"In direction {direction.name}, pieces ahead of moving piece: "
        f"{[str(piece) for piece in pieces_ahead]}",
        level=-1,
    )

    # For each piece, find the movement distance values between which the Knight
    # overlaps the piece. Store these distances (separately), along with whether
    # the distance is the start or end of an overlap range, and the piece colour.
    range_boundaries: list[_Boundary] = []
    for piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        # Variable names match what is written in Piece_movement.md.
        k_bar = (const(knight) + const(piece)) / 2
        X = knight.x
        Y = knight.y
        U = piece.x
        V = piece.y
        r = PIECE_RADIUS

        t = k_bar - Y
        knight_UR_x_coord = (X + 2 * t) / 5 + 0.2 * sqrt(5 * r**2 - (2 * X - t) ** 2)
        knight_DL_x_coord = (X + 2 * t) / 5 - 0.2 * sqrt(5 * r**2 - (2 * X - t) ** 2)
        t = k_bar - V
        piece_UR_x_coord = (U + 2 * t) / 5 + 0.2 * sqrt(5 * r**2 - (2 * U - t) ** 2)
        piece_DL_x_coord = (U + 2 * t) / 5 - 0.2 * sqrt(5 * r**2 - (2 * U - t) ** 2)

        if direction is Direction.UUR:
            overlap_start_dist = (piece_DL_x_coord - knight_UR_x_coord) * sqrt(5)
            overlap_end_dist = (piece_UR_x_coord - knight_DL_x_coord) * sqrt(5)
        else:
            overlap_start_dist = (knight_DL_x_coord - piece_UR_x_coord) * sqrt(5)
            overlap_end_dist = (knight_UR_x_coord - piece_DL_x_coord) * sqrt(5)

        assert overlap_end_dist >= overlap_start_dist

        range_boundaries.append(
            _Boundary(
                overlap_start_dist,
                _BoundaryType.START,
                piece.colour,
            )
        )
        range_boundaries.append(
            _Boundary(
                overlap_end_dist,
                _BoundaryType.END,
                piece.colour,
            )
        )

    range_boundaries.sort(key=lambda boundary: boundary.distance)

    # Prepare for iterating over the list by initialising the following:
    # Counters tracking how many same/opposite-colour pieces we are overlapping
    same_colour_overlaps = 0
    opp_colour_overlaps = 0
    # Indicator if the current region is valid to move to.
    in_valid_region = True
    # Start of the current valid region, when we in a valid region.
    region_start = 0
    # List of valid regions.
    valid_regions: list[_Movement] = []

    # Now iterate.
    for boundary in range_boundaries:
        if boundary.distance > max_distance:
            break

        if (
            boundary.boundary_type is _BoundaryType.START
            and boundary.colour is knight.colour
        ):
            same_colour_overlaps += 1
        elif boundary.boundary_type is _BoundaryType.START:
            opp_colour_overlaps += 1
        elif (
            boundary.boundary_type is _BoundaryType.END
            and boundary.colour is knight.colour
        ):
            assert same_colour_overlaps > 0
            same_colour_overlaps -= 1
        else:
            assert opp_colour_overlaps > 0
            opp_colour_overlaps -= 1

        if in_valid_region and (same_colour_overlaps > 0 or opp_colour_overlaps > 1):
            # End of valid region.
            in_valid_region = False
            # Filter out fake regions caused by float imprecision.
            if boundary.distance - region_start > 1e-5:
                valid_regions.append(
                    _Movement(region_start, boundary.distance)
                )
        elif not in_valid_region and (
            same_colour_overlaps == 0 and opp_colour_overlaps < 2
        ):
            # Start of valid region.
            in_valid_region = True
            region_start = boundary.distance

    # If in a valid region by the end, close off the end of this region.
    if in_valid_region:
        valid_regions.append(_Movement(region_start, max_distance))

    return valid_regions


def _valid_RUR_LDL_moves(knight: pieces.Piece, direction: Direction) -> list[_Movement]:
    """
    Find ranges of valid Knight movement in the direction along the RUR-LDL diagonal.

    This is "Case D" in Piece_movement.md.

    """
    if not (direction is Direction.RUR or direction is Direction.LDL):
        raise ValueError("Direction must be RUR or LDL.")

    distance_to_top_or_bottom_edge = (
        (knight.y - PIECE_DIAMETER) * sqrt(5)
        if direction is Direction.RUR
        else ((8 - PIECE_DIAMETER) - knight.y) * sqrt(5)
    )
    distance_to_left_or_right_edge = (
        (knight.x - PIECE_DIAMETER) * sqrt(5) / 2
        if direction is Direction.LDL
        else ((8 - PIECE_DIAMETER) - knight.x) * sqrt(5) / 2
    )
    max_distance = min(
        sqrt(5), distance_to_left_or_right_edge, distance_to_top_or_bottom_edge
    )

    # y+0.5x is constant along lines of this gradient. (Remember the positive
    # y direction is DOWNWARDS.)
    def const(piece: pieces.Piece) -> float:
        return piece.y + 0.5 * piece.x

    def perp(piece: pieces.Piece) -> float:
        return piece.y - 2 * piece.x

    pieces_ahead = [
        piece
        for piece in history.current_state
        if abs(const(piece) - const(knight)) < sqrt(5) * PIECE_RADIUS
        and (
            (direction is direction.RUR and perp(piece) < perp(knight))
            or (direction is direction.LDL and perp(piece) > perp(knight))
        )
    ]
    glog.log(
        f"In direction {direction.name}, pieces ahead of moving piece: "
        f"{[str(piece) for piece in pieces_ahead]}",
        level=-1,
    )

    # For each piece, find the movement distance values between which the Knight
    # overlaps the piece. Store these distances (separately), along with whether
    # the distance is the start or end of an overlap range, and the piece colour.
    range_boundaries: list[_Boundary] = []
    for piece in pieces_ahead:
        # Calculate coordinates of points which will touch.
        # Variable names match what is written in Piece_movement.md.
        k_bar = (const(knight) + const(piece)) / 2
        X = knight.x
        Y = knight.y
        U = piece.x
        V = piece.y
        r = PIECE_RADIUS

        t = k_bar - Y
        knight_UR_x_coord = (4 * X + 2 * t) / 5 + 0.4 * sqrt(
            5 * r**2 - (X - 2 * t) ** 2
        )
        knight_DL_x_coord = (4 * X + 2 * t) / 5 - 0.4 * sqrt(
            5 * r**2 - (X - 2 * t) ** 2
        )
        t = k_bar - V
        piece_UR_x_coord = (4 * U + 2 * t) / 5 + 0.4 * sqrt(5 * r**2 - (U - 2 * t) ** 2)
        piece_DL_x_coord = (4 * U + 2 * t) / 5 - 0.4 * sqrt(5 * r**2 - (U - 2 * t) ** 2)

        if direction is Direction.RUR:
            overlap_start_dist = (piece_DL_x_coord - knight_UR_x_coord) * sqrt(5) / 2
            overlap_end_dist = (piece_UR_x_coord - knight_DL_x_coord) * sqrt(5) / 2
        else:
            overlap_start_dist = (knight_DL_x_coord - piece_UR_x_coord) * sqrt(5) / 2
            overlap_end_dist = (knight_UR_x_coord - piece_DL_x_coord) * sqrt(5) / 2

        assert overlap_end_dist >= overlap_start_dist

        range_boundaries.append(
            _Boundary(
                overlap_start_dist,
                _BoundaryType.START,
                piece.colour,
            )
        )
        range_boundaries.append(
            _Boundary(
                overlap_end_dist,
                _BoundaryType.END,
                piece.colour,
            )
        )

    range_boundaries.sort(key=lambda boundary: boundary.distance)

    # Prepare for iterating over the list by initialising the following:
    # Counters tracking how many same/opposite-colour pieces we are overlapping
    same_colour_overlaps = 0
    opp_colour_overlaps = 0
    # Indicator if the current region is valid to move to.
    in_valid_region = True
    # Start of the current valid region, when we in a valid region.
    region_start = 0
    # List of valid regions.
    valid_regions: list[_Movement] = []

    # Now iterate.
    for boundary in range_boundaries:
        if boundary.distance > max_distance:
            break

        if (
            boundary.boundary_type is _BoundaryType.START
            and boundary.colour is knight.colour
        ):
            same_colour_overlaps += 1
        elif boundary.boundary_type is _BoundaryType.START:
            opp_colour_overlaps += 1
        elif (
            boundary.boundary_type is _BoundaryType.END
            and boundary.colour is knight.colour
        ):
            assert same_colour_overlaps > 0
            same_colour_overlaps -= 1
        else:
            assert opp_colour_overlaps > 0
            opp_colour_overlaps -= 1

        if in_valid_region and (same_colour_overlaps > 0 or opp_colour_overlaps > 1):
            # End of valid region.
            in_valid_region = False
            # Filter out fake regions caused by float imprecision.
            if boundary.distance - region_start > 1e-5:
                valid_regions.append(
                    _Movement(region_start, boundary.distance)
                )
        elif not in_valid_region and (
            same_colour_overlaps == 0 and opp_colour_overlaps < 2
        ):
            # Start of valid region.
            in_valid_region = True
            region_start = boundary.distance

    # If in a valid region by the end, close off the end of this region.
    if in_valid_region:
        valid_regions.append(_Movement(region_start, max_distance))

    return valid_regions


def _valid_pawn_moves(pawn: pieces.Piece) -> dict[Direction, _Movement]:
    """
    Pawns cannot yet be promoted, or capture en passant.

    """
    assert pawn.piece_type is PieceType.PAWN

    valid_moves: dict[Direction, _Movement] = {}

    if pawn.colour is Colour.BLACK:
        valid_moves[Direction.D] = _valid_up_or_down_move(pawn, Direction.D)
        if valid_dl_capture := _valid_DL_UR_pawn_capture(pawn, Direction.DL):
            valid_moves[Direction.DL] = valid_dl_capture
        if valid_dr_capture := _valid_UL_DR_pawn_capture(pawn, Direction.DR):
            valid_moves[Direction.DR] = valid_dr_capture
    else:
        valid_moves[Direction.U] = _valid_up_or_down_move(pawn, Direction.U)
        if valid_ur_capture := _valid_DL_UR_pawn_capture(pawn, Direction.UR):
            valid_moves[Direction.UR] = valid_ur_capture
        if valid_ul_capture := _valid_UL_DR_pawn_capture(pawn, Direction.UL):
            valid_moves[Direction.UL] = valid_ul_capture

    return valid_moves


def _valid_rook_moves(rook: pieces.Piece) -> dict[Direction, _Movement]:
    """Return a list of valid moves for a given rook."""
    assert rook.piece_type is PieceType.ROOK

    rook_moves = {}
    rook_moves[Direction.U] = _valid_up_or_down_move(rook, Direction.U)
    rook_moves[Direction.D] = _valid_up_or_down_move(rook, Direction.D)
    rook_moves[Direction.L] = _valid_left_or_right_move(rook, Direction.L)
    rook_moves[Direction.R] = _valid_left_or_right_move(rook, Direction.R)
    return rook_moves


def _valid_queen_moves(queen: pieces.Piece) -> dict[Direction, _Movement]:
    """Return a list of valid moves for a given queen."""
    assert queen.piece_type is PieceType.QUEEN

    queen_moves = {}
    queen_moves[Direction.U] = _valid_up_or_down_move(queen, Direction.U)
    queen_moves[Direction.D] = _valid_up_or_down_move(queen, Direction.D)
    queen_moves[Direction.L] = _valid_left_or_right_move(queen, Direction.L)
    queen_moves[Direction.R] = _valid_left_or_right_move(queen, Direction.R)
    queen_moves[Direction.DL] = _valid_DL_UR_move(queen, Direction.DL)
    queen_moves[Direction.UR] = _valid_DL_UR_move(queen, Direction.UR)
    queen_moves[Direction.UL] = _valid_UL_DR_move(queen, Direction.UL)
    queen_moves[Direction.DR] = _valid_UL_DR_move(queen, Direction.DR)
    return queen_moves


def _valid_king_moves(king: pieces.Piece) -> dict[Direction, _Movement]:
    """Return a list of valid moves for a given king."""
    assert king.piece_type is PieceType.KING

    king_moves = {}
    king_moves[Direction.U] = _valid_up_or_down_move(king, Direction.U)
    king_moves[Direction.D] = _valid_up_or_down_move(king, Direction.D)
    king_moves[Direction.L] = _valid_left_or_right_move(king, Direction.L)
    king_moves[Direction.R] = _valid_left_or_right_move(king, Direction.R)
    king_moves[Direction.DL] = _valid_DL_UR_move(king, Direction.DL)
    king_moves[Direction.UR] = _valid_DL_UR_move(king, Direction.UR)
    king_moves[Direction.UL] = _valid_UL_DR_move(king, Direction.UL)
    king_moves[Direction.DR] = _valid_UL_DR_move(king, Direction.DR)
    return king_moves


def _valid_bishop_moves(bishop: pieces.Piece) -> dict[Direction, _Movement]:
    """Return a list of valid moves for a given bishop."""
    assert bishop.piece_type is PieceType.BISHOP

    bishop_moves = {}
    bishop_moves[Direction.DL] = _valid_DL_UR_move(bishop, Direction.DL)
    bishop_moves[Direction.UR] = _valid_DL_UR_move(bishop, Direction.UR)
    bishop_moves[Direction.UL] = _valid_UL_DR_move(bishop, Direction.UL)
    bishop_moves[Direction.DR] = _valid_UL_DR_move(bishop, Direction.DR)
    return bishop_moves


def _valid_knight_moves(knight: pieces.Piece) -> dict[Direction, list[_Movement]]:
    assert knight.piece_type is PieceType.KNIGHT

    knight_moves = {}
    knight_moves[Direction.UUL] = _valid_UUL_DDR_moves(knight, Direction.UUL)
    knight_moves[Direction.DDR] = _valid_UUL_DDR_moves(knight, Direction.DDR)
    knight_moves[Direction.LUL] = _valid_LUL_RDR_moves(knight, Direction.LUL)
    knight_moves[Direction.RDR] = _valid_LUL_RDR_moves(knight, Direction.RDR)
    knight_moves[Direction.UUR] = _valid_UUR_DDL_moves(knight, Direction.UUR)
    knight_moves[Direction.DDL] = _valid_UUR_DDL_moves(knight, Direction.DDL)
    knight_moves[Direction.RUR] = _valid_RUR_LDL_moves(knight, Direction.RUR)
    knight_moves[Direction.LDL] = _valid_RUR_LDL_moves(knight, Direction.LDL)

    return knight_moves

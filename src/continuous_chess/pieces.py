from __future__ import annotations

__all__ = (
    "PIECE_RADIUS",
    "PIECE_DIAMETER",
    "PIECE_NAMES",
    "PieceType",
    "Piece",
)

from enum import Enum

from .datatypes import Colour, Coordinate

PIECE_RADIUS = 0.25
PIECE_DIAMETER = 2 * PIECE_RADIUS

PIECE_NAMES = [
    "pawn-white",
    "rook-white",
    "knight-white",
    "bishop-white",
    "king-white",
    "queen-white",
    "pawn-black",
    "rook-black",
    "knight-black",
    "bishop-black",
    "king-black",
    "queen-black",
]


class PieceType(Enum):
    PAWN = "pawn"
    ROOK = "rook"
    KNIGHT = "knight"
    BISHOP = "bishop"
    KING = "king"
    QUEEN = "queen"

    def __str__(self) -> str:
        if self is PieceType.KNIGHT:
            return "N"
        elif self is PieceType.PAWN:
            return "p"
        else:
            return self.name[0]


class Piece:
    """
    Object representing a piece.

    """

    def __init__(
        self, piece_type: PieceType, colour: Colour, start: Coordinate
    ) -> None:
        self.piece_type = piece_type
        self.colour = colour
        self.location = start
        self.taken: bool = False  # Whether the piece has been taken.
        self.moved: bool = False  # Whether the piece has been moved.

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Piece):
            return False

        # Don't bother checking all attributes, it's not necessary (probably). The
        # only way two pieces can be at the same location is during a capture, and
        # a piece cannot capture a piece of the same colour, so it suffices to only
        # check location and colour.
        return self.colour == other.colour and self.location == other.location

    def __repr__(self) -> str:
        colour = self.colour.name[0]
        return f"{colour}{self.piece_type}@{self.location}"

    def __str__(self) -> str:
        return (
            f"{self.colour.name} {self.piece_type.name} "
            f"at ({self.location.x}, {self.location.y})"
        )

    def __hash__(self) -> int:
        """
        Doesn't need to be secure. This is only defined to enable using Pieces
        as dictionary keys. Pieces MUST NOT BE MODIFIED for the lifespan of the
        dictionary object; this "feature" of piece objects should only be used
        for dictionaries with short lifespans.

        """

        # Piece coordinates are unique to 1dp, so create a 4-digit number using
        # these. First two digits are the x-coordinate*10, second two are
        # y-coordinate*10.
        return 100 * int(10 * self.x) + int(10 * self.y)

    @property
    def x(self):
        return self.location.x

    @property
    def y(self):
        return self.location.y

    def distance_from(self, other: Piece | Coordinate) -> float:
        """Calculate distance from this piece to another piece or coordinate."""
        if isinstance(other, Piece):
            return self.location.distance_from(other.location)
        elif isinstance(other, Coordinate):
            return self.location.distance_from(other)
        else:
            raise ValueError(f"Unexpected type: {type(other)}.")

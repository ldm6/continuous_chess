# Board module - handles board drawing and also some piece location calculations.

__all__ = (
    "WIDTH",
    "HEIGHT",
    "load_images",
    "draw_game_state",
    "piece_at",
    "pieces_within",
)

import pathlib

from typing import Optional

import pygame

from . import history, pieces
from .datatypes import Coordinate
from .pieces import PIECE_RADIUS, Piece

WIDTH = 512
HEIGHT = 512
DIMENSION = 8
SQUARE_SIZE = HEIGHT // DIMENSION
IMAGES = {}
IMAGE_DIR = pathlib.Path(__file__).parents[2] / "images"


def piece_at(location: Coordinate) -> Optional[Piece]:
    for piece in history.current_state:
        if piece.location.distance_from(location) < PIECE_RADIUS:
            return piece
    else:
        return None


def pieces_within(distance: float, location: Coordinate) -> list[Piece]:
    pieces_within_distance = []
    for piece in history.current_state:
        if piece.distance_from(location) <= distance:
            pieces_within_distance.append(piece)

    return pieces_within_distance


def load_images():
    for piece in pieces.PIECE_NAMES:
        IMAGES[piece] = pygame.transform.scale(
            pygame.image.load(IMAGE_DIR / f"{piece}.png"), (SQUARE_SIZE, SQUARE_SIZE)
        )


def _draw_squares(screen: pygame.Surface):
    colours = (pygame.Color("white"), pygame.Color("dark green"))
    for row in range(DIMENSION):
        for col in range(DIMENSION):
            colour = colours[(row + col) % 2]
            pygame.draw.rect(
                screen,
                colour,
                pygame.Rect(
                    col * SQUARE_SIZE, row * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE
                ),
            )


def _draw_pieces(screen: pygame.Surface):
    for piece in history.current_state:
        screen.blit(
            IMAGES[f"{piece.piece_type.value}-{piece.colour.value}"],
            pygame.Rect(
                (
                    (piece.location.x - 0.5) * SQUARE_SIZE,
                    (piece.location.y - 0.5) * SQUARE_SIZE,
                    SQUARE_SIZE,
                    SQUARE_SIZE,
                )
            ),
        )


def draw_game_state(screen: pygame.Surface):
    _draw_squares(screen)
    _draw_pieces(screen)

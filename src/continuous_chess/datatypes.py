from __future__ import annotations

__all__ = ("Coordinate",)

import math as maths
from enum import Enum
from typing import NamedTuple


class Coordinate(NamedTuple):
    """
    Board coodinate. Note that the "origin" is the TOP-left corner.
    """

    x: float
    y: float

    def distance_from(self, other: Coordinate) -> float:
        return maths.hypot(self.x - other.x, self.y - other.y)

    def __str__(self) -> str:
        return f"({self.x},{self.y})"


class Colour(Enum):
    WHITE = "white"
    BLACK = "black"

    def other(self) -> Colour:
        if self is Colour.WHITE:
            return Colour.BLACK
        else:
            return Colour.WHITE

# History module - handles past *and current* board states.

from __future__ import annotations

# Note that current_state should not be imported explicitly.
# (i.e. do not do `from history import current_state`)
__all__ = ("GameState", "current_state", "game_history")

from typing import Generator, Optional

from . import game_log as glog
from . import pieces
from .datatypes import Colour, Coordinate
from .pieces import Piece, PieceType


class GameState:
    def __init__(self, board_state: list[pieces.Piece], turn: Colour):
        self.board_state = board_state
        self.turn = turn  # Colour of player who is moving next.
        # TODO: Way to track if en passant is legal

    def __iter__(self) -> Generator[pieces.Piece, None, None]:
        for piece in self.board_state:
            yield piece

    def __str__(self) -> str:
        return f"GameState({[piece for piece in self]})"


"""
Starting position of the pieces.

Order does not matter, so the data would be better described by a Set, but
there are few enough elements that the hashing overheads would be undesirable.

"""
_STARTING_CONFIGURATION = [
    Piece(PieceType.ROOK, Colour.WHITE, Coordinate(0.5, 7.5)),
    Piece(PieceType.KNIGHT, Colour.WHITE, Coordinate(1.5, 7.5)),
    Piece(PieceType.BISHOP, Colour.WHITE, Coordinate(2.5, 7.5)),
    Piece(PieceType.QUEEN, Colour.WHITE, Coordinate(3.5, 7.5)),
    Piece(PieceType.KING, Colour.WHITE, Coordinate(4.5, 7.5)),
    Piece(PieceType.BISHOP, Colour.WHITE, Coordinate(5.5, 7.5)),
    Piece(PieceType.KNIGHT, Colour.WHITE, Coordinate(6.5, 7.5)),
    Piece(PieceType.ROOK, Colour.WHITE, Coordinate(7.5, 7.5)),
    Piece(PieceType.PAWN, Colour.WHITE, Coordinate(0.5, 6.5)),
    Piece(PieceType.PAWN, Colour.WHITE, Coordinate(1.5, 6.5)),
    Piece(PieceType.PAWN, Colour.WHITE, Coordinate(2.5, 6.5)),
    Piece(PieceType.PAWN, Colour.WHITE, Coordinate(3.5, 6.5)),
    Piece(PieceType.PAWN, Colour.WHITE, Coordinate(4.5, 6.5)),
    Piece(PieceType.PAWN, Colour.WHITE, Coordinate(5.5, 6.5)),
    Piece(PieceType.PAWN, Colour.WHITE, Coordinate(6.5, 6.5)),
    Piece(PieceType.PAWN, Colour.WHITE, Coordinate(7.5, 6.5)),
    Piece(PieceType.ROOK, Colour.BLACK, Coordinate(0.5, 0.5)),
    Piece(PieceType.KNIGHT, Colour.BLACK, Coordinate(1.5, 0.5)),
    Piece(PieceType.BISHOP, Colour.BLACK, Coordinate(2.5, 0.5)),
    Piece(PieceType.QUEEN, Colour.BLACK, Coordinate(3.5, 0.5)),
    Piece(PieceType.KING, Colour.BLACK, Coordinate(4.5, 0.5)),
    Piece(PieceType.BISHOP, Colour.BLACK, Coordinate(5.5, 0.5)),
    Piece(PieceType.KNIGHT, Colour.BLACK, Coordinate(6.5, 0.5)),
    Piece(PieceType.ROOK, Colour.BLACK, Coordinate(7.5, 0.5)),
    Piece(PieceType.PAWN, Colour.BLACK, Coordinate(0.5, 1.5)),
    Piece(PieceType.PAWN, Colour.BLACK, Coordinate(1.5, 1.5)),
    Piece(PieceType.PAWN, Colour.BLACK, Coordinate(2.5, 1.5)),
    Piece(PieceType.PAWN, Colour.BLACK, Coordinate(3.5, 1.5)),
    Piece(PieceType.PAWN, Colour.BLACK, Coordinate(4.5, 1.5)),
    Piece(PieceType.PAWN, Colour.BLACK, Coordinate(5.5, 1.5)),
    Piece(PieceType.PAWN, Colour.BLACK, Coordinate(6.5, 1.5)),
    Piece(PieceType.PAWN, Colour.BLACK, Coordinate(7.5, 1.5)),
]

current_state = GameState(_STARTING_CONFIGURATION, Colour.WHITE)

game_history: list[GameState] = [current_state]


def update_state_from_pieces(
    new_piece_positions: list[pieces.Piece], turn: Optional[Colour] = None
) -> None:
    """
    Update game state using a list of Pieces to define the new position.

    This is treated as a new move: the previous board state is added to history,
    and the current player is switched to the other colour.

    """
    global current_state, game_history

    if turn is None:
        turn = current_state.turn.other()

    new_state = GameState(new_piece_positions, turn)
    glog.log(f"new_state={str(new_state)}", level=1)

    current_state = new_state
    game_history.append(new_state)


def undo_move() -> None:
    """
    Undo the latest move.

    This cannot be reverted; there is no "redo" functionality at present.

    """
    global current_state, game_history

    if len(game_history) < 2:
        # No-op if there are no moves to undo.
        glog.log("Attempted to undo move, but no moves left to undo.")
        return

    # Remove the latest move from the game history.
    game_history.pop()
    current_state = game_history[-1]
    glog.log(
        "Move undone. Now on half-move #{}.{}.".format(
            (len(game_history) + 1) // 2, 5 * ((len(game_history) + 1) % 2)
        )
    )

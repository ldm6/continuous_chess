#!/usr/bin/env python3

import pygame

from continuous_chess import board
from continuous_chess import game_log as glog
from continuous_chess import history, moves, pieces
from continuous_chess.datatypes import Coordinate

MAX_FPS = 60


def main() -> None:
    pygame.init()
    screen = pygame.display.set_mode((board.WIDTH, board.HEIGHT))
    clock = pygame.time.Clock()
    screen.fill(pygame.Color("white"))
    board.load_images()
    prev_selection = Coordinate(-1, -1)
    player_clicks: list[Coordinate] = []  # Max length: 2
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                location = pygame.mouse.get_pos()
                new_selection = Coordinate(
                    location[0] / board.SQUARE_SIZE, location[1] / board.SQUARE_SIZE
                )
                glog.log(f"{new_selection = }", level=-2)

                if new_selection.distance_from(prev_selection) < pieces.PIECE_RADIUS:
                    # Clicked on same piece twice.
                    glog.log("Cancelled selection.")
                    # Reset variables tracking clicks.
                    prev_selection = Coordinate(-1, -1)
                    player_clicks = []
                else:
                    # If a piece was clicked, store the piece's location instead
                    # of the click location, as the click location is irrelevant.
                    selected_piece = board.piece_at(new_selection)
                    selection_location = (
                        selected_piece.location
                        if selected_piece is not None
                        else new_selection
                    )

                    #  Handle a first click on a location without a movable piece.
                    if len(player_clicks) == 0:
                        if selected_piece is not None:
                            if selected_piece.colour is not history.current_state.turn:
                                glog.log("Clicked opponent's piece, ignoring click.")
                                continue
                        else:
                            # Player has clicked on an empty tile as their first click.
                            glog.log("Clicked empty location.")
                            continue

                    # Handle all other clicks.
                    prev_selection = selection_location
                    # First check if the player has changed their mind and clicked on
                    # another piece of theirs, having selected one previously.
                    if (
                        len(player_clicks) > 0
                        and selected_piece is not None
                        and selected_piece.colour is history.current_state.turn
                    ):
                        glog.log(
                            f"Player {history.current_state.turn} clicked a different "
                            "piece of their own colour, updating selected piece."
                        )
                        player_clicks = [selection_location]
                        continue

                    player_clicks.append(selection_location)
                    if len(player_clicks) > 1:
                        # 2nd successful click. Make the move.
                        assert len(player_clicks) == 2
                        moves.make_move(*player_clicks)
                        # Reset variables tracking clicks.
                        prev_selection = Coordinate(-1, -1)
                        player_clicks = []
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_z:
                    history.undo_move()
                    player_clicks = []

        board.draw_game_state(screen)
        clock.tick(MAX_FPS)
        pygame.display.flip()


if __name__ == "__main__":
    glog.set_debug_level(0)
    main()

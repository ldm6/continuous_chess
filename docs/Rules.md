# Gameplay (differences to regular chess)

Pieces are circular with diameter 0.5 units on an 8x8 unit board. The grid squares serve no purpose other than a guide.

Pieces move in their usual directions: 8 directions for K/Q/N, 4 directions for B/R, 1 for pawns when not capturing. Pawns capture diagonally.

Pieces can move up to their usual max distance, up to but not past a piece of the same colour. Note that knights can move $\sqrt{5}$ units, pawns generally move up to 1 unit except on their first move (up to 2) and when capturing (up to $\sqrt{5}$). Kings move up to 1 when moving U/D/L/R and $\sqrt{2}$ units when moving diagonally.

TODO: pics

There is currently no minimum move distance. This rule may be amended later.

## Captures

Captures are done by moving in a way which overlaps an enemy piece. At most one piece can be captured at a time. If you attempt to move in a way that would capture two pieces, the second piece would block your distance as if it were a piece of your own colour.

You cannot move through pieces; if you move along a path where a piece would be captured, you cannot move completely past the piece.

Knights can jump over anything, but cannot land on a piece of their own colour, and cannot land on multiple enemy pieces at the same time.

## Castling

Castling requires the King and Rook to have not yet moved.

When castling Kingside, the King moves between 1.5-2 units and the Rook moves to be exactly one unit the other side of the king. No pieces may be blocking the 0.5-unit tall area between the King and Rook's starting locations. When castling Queenside the only difference is that the King moves from 2.5-3 units.

@@@ pic


## Promotion

Pawns promote when their centre point moves within 1 unit of the furthest edge of the board. The promoted piece remains in the same place during the promotion.

## Stalemate

Stalemate is the same as regular chess, i.e. if it is a player's turn and they have no moves available, the game ends in a draw.

## En Passant

When a pawn is moved for the first time and the enemy attempts to capture it with a pawn the following move, the original Pawn's "hitbox" is treated as the entire path from its starting position to its current position.

## Other notable differences

Kings may not move through check. Note that unlike regular chess, this is applicable even when not castling, as an enemy piece's range could poke into the King's path.

TODO: picture

When a double check occurs in regular chess, the King must move. This is not true in continuous chess as a single piece may block attacks from two directions.

TODO: picture

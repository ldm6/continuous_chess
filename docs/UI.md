In general, when selecting a target location, the selection will be snapped to the "nearest" line the piece can move on. However there are some simplifications to this to make calculations easier:

- When selecting a target position for a Queen or King, the regions used to determine the snapping are bound by the lines $y=\pm \frac{1}{2}x$ and $y=\pm 2x$ from the reference frame where the moving piece is centred at the origin.
- For Knights, if moving in the right or left quarters (split by diagonals through the start position), snap with fixed column. Else snap with fixed row.

In the below diagram, click locations are marked with a cross, and the arrows point to where the piece will attempt to move.

![](docs/imgs/piece-click-regions.png)

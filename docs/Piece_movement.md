The following document is written before pawn captures, en passant, castling and promotion have been implemented. These concepts are ignored.

**Note that the coordinate system used is one where the positive y direction is *downwards*.**

Note some formatting may seem off - GitLab allows a maximum of 50 LaTeX equations per document and refuses to render any more to avoid risk of DOS. Because of this, some simple equations/expressions will not be written in LaTeX. Also note that Markdown's subset of LaTeX does not offer control over formatting such as spacing and alignment.

# Non-Knight pieces

All pieces (ignoring Knights for now) can move in up to eight directions. In each direction they have a maximum distance they can move.

Pieces can move up to touching a piece of the same colour. If they move through a piece of the enemy colour, that piece must be captured, and the movement must stop before overlapping any other enemy pieces along the path (i.e. at most 1 capture can occur). It is not possible to move through a piece without capturing.

## Orthogonal movement

The algorithm to determine maximum movement distance is the same for moving upwards, downwards, left and right. The algorithm below is written from the perspective of a piece moving upwards, but the same logic is used for movement in any of the other three orthogonal directions.

### Algorithm
To calculate the range of movement which is valid in a given direction, note that there are five things which put an upper limit on your movement distance:
- The moving piece may have a limit on its movement distance (if it is a Pawn or King)
- The edge of the board
- The trailing edge of a piece being captured (i.e. you cannot pass completely through a piece while capturing it)
- A piece of the opposite colour, if it is the second piece you would touch
- A piece of the same colour.

The idea of the algorithm is to independently calculate these five values and return the lowest one.

1. Initialise an array of distance upper bounds.
2. If the piece type has a maximum movement distance (i.e. the moving piece is a Pawn or King), add that value to the upper-bound array.
3. Calculate the distance to the edge of the board, and add that value to the upper-bound array.
4. Find all pieces with x-coord within PIECE_DIAMETER of the moving piece which are above the moving piece.
5. For each such piece, calculate the "touching distance" to the moving piece. For these calculations ignore all other pieces, even if they may potentially block the movement. This is done by calculating the coordinates of the points on the circles which will touch.
  - The touching points on the two pieces will have x-coord halfway between the pieces' x-coords.
  - Calculate the y-coordinates of the touching points. See Appendix A.
  - Find the difference between the y-coordinate values. This is the touching distance between this piece and the moving piece.
6. Using these distances, identify the "blocking pieces", i.e. the pieces we cannot move past:
  - Sort the pieces by touching distance.
  - Find the first same-colour piece in the list. Add its touching distance to the upper-bound array.
  - Find the second opposite-colour piece in the list. Add its touching distance to the upper-bound array.
    - For pawns moving forward, all pieces block movement, even if the first piece encountered is an enemy piece.
7. For each piece found in step 3, calculate the "overlap distance" from the moving piece. For these calculations ignore all other pieces, even if they may potentially block the movement. This is done by calculating the coordinates of the points on the circles which touch when the moving piece has overlapped as far as it can while still touching the non-moving piece. The calculations are the same as step 5, see Appendix A.
8. Add the smallest maximum overlap distance to the distance bound array. Note that even if the first piece is of the same colour as the moving piece, its overlap distance can never be reached (as that would involve moving through a piece of the same colour), so its distance is a strictly stronger bound than the max-overlap distance of the closest opposite-colour piece.
9. Return the minimum of the elements in the distance bound array.

## Diagonal movement

The logic for diagonal movement is the exact same as orthogonal movement. However, the distance calculations are harder.

In step 2, there are two edges of the board a piece could hit. The distances to both should be added to the array of upper bounds.

In steps 5 and 7, the concepts are the same, but the calculations are different. See Appendix B.


# Knight movement

Knights jump in a straight line along their usual angles, up to $\sqrt{5}$ units. They can land anywhere on that path with the same restrictions as other pieces, i.e. they cannot land on a piece of the same colour, and cannot land on two enemy pieces. If they land on one enemy piece, the Knight captures that piece.

Unlike other pieces, for each direction, their set of valid movements will be a *list of ranges* in each direction.

## Algorithm

The idea of this algorithm is to find regions where the Knight would land on at most 1 enemy piece and at most 0 friendly pieces.

(wlog considering just one of the Knight's movement directions: the direction moving up-right with gradient 2.)

1. Calculate the maximum distance the Knight could move ignoring all other pieces. This is equal to $\text{min}(\sqrt{5}, \text{<distance to edge of board>})$. As with diagonal movement for other pieces, there are two edges of the board which must be considered.
2. In the same manner as step 1 of the above algorithm for "normal" moving pieces, find the pieces which will overlap the Knight's path in the direction of movement. See Appendix C for how to calculate this.
3. In the same manner as steps 2 and 5 of the above algorithm for "normal" moving pieces, for each piece find the two distances between which the Knight's movement will overlap the given piece. These are the distances between the pairs of points previously called "touching points" and "maximum overlap points". See Appendix D for calculations.
4. For each of these distances, create a tuple containing (a) the distance, (b) whether it is the start or end of a range, (c) the piece colour. Add all of these to a list. (note that each range's start/end positions are now disconnected - it is not relevant which start and end points match)
5. Sort this list by distance.
6. Initialise the following variables:
   - Two counters, one for each colour - these will count how many pieces we are in the overlap range of.
   - A variable indicating if the region we are currently checking is a valid region to move to.
   - A "range start" variable initialised to 0. When in a valid region, this will track the start of the region.
   - An array which will contain a list of valid regions.
7. Iterate through the list, taking the following actions for each point.
   a- If the point is beyond the max movement distance, break from the loop. Take no further actions for this point.
   b- If the point is a start point, increment the counter for that colour. If the point is an end point, decrement the corresponding counter.
   c- Check the status of the counters.
      - If in a valid region and (the same-colour counter is >0 OR the opposite-colour counter is >1): this is the end of a valid region. Read the "range start" variable and, using the current point's position as the end value, store this range in the "valid regions" array.
      - Else if in an invalid region and (the same-colour counter is 0 AND the opposite-colour counter is <2): this is the start of a valid region. Save the current location into the "range start" variable.
      - Else, take no action.
8. If in an invalid region at the end of the loop, return the array of valid regions.
9. If in a valid region at the end of the loop, use the max movement distance as the end value of a range. This and the "range start" variable form one final range which is appended to the array of valid regions. Then return the array.


# Pawn captures

Pawn captures use the same calculations as in steps 1-5 of the Knight move algorithm above. However, no iteration is done, and only zero or one movement range(s) is/are returned.

6. If any of the following hold, no capture (and hence no movement in this direction) is possible:
   - There are no pieces in the direction of movement
   - The piece colour at the first boundary is the same colour as the moving Pawn
   - The first boundary is beyond the max movement distance
   - The first boundary is beyond the distance to the edge of the board
7. The first boundary must be for an opposite-colour piece, so this is the start of the valid movement range.
8. The end of the movement range is defined by the lowest of the following:
   - The max movement distance
   - The distance to the edge of the board
   - The second boundary (regardless of type or piece colour)


# Appendix A: Touching coordinates for orthogonal movement

This section assumed wlog the moving piece is moving upwards, and touching a piece above it.

The x-coordinate of the touching points is the mean of the x-coordinates of the two pieces.

To find the y-coordinate of the touching point on the moving piece, look at the following triangle:

![](docs/imgs/orthogonal-touch-coords.png)

The centre of the moving piece is at (X, Y). Denoting the difference between the touching point's x-coordinate and X as $\Delta x$, the y-coordinate of the touching point on the moving piece is $Y - \sqrt{r^2 - (\Delta x)^2}$. Similarly if the non-moving piece is centred at (U, V), the y-coordinate of its touching point is $V + \sqrt{r^2 - (\Delta x)^2}$.

When calculating maximum overlap distance, the calculations are the same. The y-coordinate of the max overlap point on the moving piece is $Y + \sqrt{r^2 - (\Delta x)^2}$ and the y-coordinate of the max overlap point on the static piece is $V - \sqrt{r^2 - (\Delta x)^2}$.

# Appendix B: Touching coordinates for diagonal movement.

While orthogonal directions have either an x or y value constant, diagonal directions have $(y-x)$ or $(y+x)$ constant. For the remainder of this section, assume wlog the moving piece is moving downwards and to the right. For pieces moving in this direction, $y-x$ is constant. In the code, this value is sometimes referred to as the "const" value of a given coordinate or piece. The other value (in this case $y+x$) is referred to as the "perp" value.

For step 4, to determine which pieces the moving piece would touch if it moved in this direction, note that objects intersect if their $y-x$ value is within $2r \sqrt{2}$:

![](docs/imgs/diagonal-touch-distance.png)

For steps 5 and 7, the touching and overlap points need to be found. These are done by solving simultaneous equations.

First, the x-coordinate of the touching point on the moving piece.

Say the moving piece is centred at (X, Y) on the line $y-x=k_1$ and the static piece is centred at (U, V) on the line $y-x=k_2$. Let $\bar{k}=\frac{k_1 + k_2}{2}$. The touching point's coordinates are found by solving the simultaneous equations:
$$
(x-X)^2 + (y-Y)^2 = r^2 \\
y-x = \bar{k}
$$

![](docs/imgs/diagonal-touch-coords.png)

The two solutions of this are the x-coordinates of the touching point and max overlap point. These can be solved as follows:

$$
(x-X)^2 + (x+\bar{k}-y)^2 = r^2
$$

Let $t = Y - \bar{k}$

$$
(x-X)^2 + (x-t)^2 = r^2  \\
x^2 - 2Xx + X^2 + x^2 - 2tx + t^2 = r^2  \\
2x^2 + (-2X-2t)x + (X^2 + t^2 - r^2) = 0  \\
x = \frac{2(X+t) \pm \sqrt{(2X+2t)^2 - 8(X^2+t^2-r^2)}}{4}  \\
x = \frac{X+t}{2} \pm \frac{1}{4} \sqrt{4X^2+8Xt+4t^2 -8X^2-8t^2+8r^2}  \\
x = \frac{X+t}{2} \pm \frac{1}{2} \sqrt{X^2+2Xt+t^2 -2X^2-2t^2+2r^2}  \\
x = \frac{X+t}{2} \pm \frac{1}{2} \sqrt{2r^2 - (X-t)^2}  \\
$$

The "+" version is the touch point's x-coordinate, the "-" version is the max overlap point's x-coordinate. By symmetry, the corresponding x-coordinates on the static piece are
$$
x = \frac{U+t}{2} \mp \frac{1}{2} \sqrt{2r^2 - (U-t)^2}  \\
$$
where $t=V-\bar{k}$.

Because these points all line on $y-x=\bar{k}$, the y-coordinates are $\bar{k}$ more than the x-coordinates.

## Pieces moving upwards and to the right

For pieces moving upwards and to the right, the constant function is now y+x, and the equations are the same but with $t=\bar{k}-Y$ and $t=\bar{k}=V$.

# Appendix C: Identifying pieces in a Knight's path

Knights can move along paths where any of the four values are constant:
- $y-2x$
- $y-\frac{1}{2}x$
- $y+2x$
- $y+\frac{1}{2}x$

These cases will henceforth be called "Case A" - "Case D" respectively. The directions corresponding to each const value are:
| case | const | directions |
| ---- | ----- | ---------- |
| Case A | y-2x  | UUL and DDR |
| Case B | y-0.5x  | LUL and RDR |
| Case C | y+2x  | UUR and DDL  |
| Case D | y+0.5x  | RUR and LDL |

In step 2, we must determine which pieces are in the Knight's path. Observe that in Case A, objects intersect the Knight's path if their $y-2x$ value is within $2r \sqrt{5}$ of the Knight's:

![](docs/imgs/ddr-touch-distance.png)

By symmetry this is true for case C.

By a similar calculation, for cases B and D, pieces intersect the Knight's path if their $y - \frac{1}{2}x$ value (or $y + \frac{1}{2}$) is within $\frac{\sqrt{5}}{2}r$ of the Knight's.

# Appendix D: Touch and overlap coordinates for pieces on a Knight's path

This section will largely mirror the calculations in Appendix B.

## Case A

First consider case A as described in Appendix C. The simultaneous equations to solve are:
$$
(x-X)^2 + (y-Y)^2 = r^2 \\
y-2x = \bar{k}
$$

These can be solved as follows:

$$
(x-X)^2 + (2x+\bar{k}-y)^2 = r^2
$$

Let $t = Y - \bar{k}$

$$
(x-X)^2 + (2x-t)^2 = r^2  \\
x^2 - 2Xx + X^2 + 4x^2 - 4tx + t^2 = r^2  \\
5x^2 + (-2X-4t)x + (X^2 + t^2 - r^2) = 0  \\
x = \frac{2X+4t \pm \sqrt{(2X+4t)^2 - 20(X^2+t^2-r^2)}}{10}  \\
x = \frac{X+2t}{5} \pm \frac{1}{10} \sqrt{4X^2+16Xt+16t^2 -20X^2-20t^2+20r^2}  \\
x = \frac{X+2t}{5} \pm \frac{1}{5} \sqrt{X^2+4Xt+4t^2 -5X^2-5t^2+5r^2}  \\
x = \frac{X+2t}{5} \pm \frac{1}{5} \sqrt{5r^2 - (2X-t)^2}  \\
$$

The "+" version is the touch point's x-coordinate, the "-" version is the max overlap point's x-coordinate. By symmetry, the corresponding x-coordinates on the static piece are
$$
x = \frac{U+2t}{5} \mp \frac{1}{5} \sqrt{5r^2 - (2U-t)^2}  \\
$$
where $t=V-\bar{k}$.

Because these points all line on $y-2x=\bar{k}$, the y-coordinates are $\bar{k}$ more than double the x-coordinates.

## Case C

Case C is similar to Case A, however the second equation in the system of simultaneous equations is $y+2x = \bar{k}$. The solving procedure is the same and gives the same formulae, except with $t=\bar{k}-Y$ for the Knight and $t=\bar{k}-V$ for the other piece.

## Cases B and D

Calculations for Cases B and D are omitted here, but are largely the same. The solution for Case B is (with $t=Y-\bar{k}$):

$$
x=\frac{4X+2t}{5} \pm \frac{2}{5} \sqrt{5r^2 - (X-2t)^2}
$$

For Case D the solution is the same but with $t=\bar{k}-Y$.

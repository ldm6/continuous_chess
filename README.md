To run:
- Create a virtual environment and activate it.
- Install requirements with `pip install -r requirements-dev.txt`.
- Run `src/main.py`.
